﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Contracts.EntityGateways {

    public interface ICreateEntityGateway<TEntity> : IEntityGateway where TEntity : IEntity {

        TEntity Create(TEntity creature);

    }

}