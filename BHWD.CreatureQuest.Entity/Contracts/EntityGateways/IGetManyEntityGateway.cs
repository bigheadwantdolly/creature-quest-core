﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Contracts.EntityGateways {

    public interface IGetManyEntityGateway<TEntity> : IEntityGateway where TEntity : IEntity {

        List<TEntity> GetAll();

    }

}