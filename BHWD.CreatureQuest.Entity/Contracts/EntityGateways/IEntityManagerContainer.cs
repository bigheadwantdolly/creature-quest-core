﻿namespace BHWD.CreatureQuest.Entity.Contracts.EntityGateways {

    public interface IEntityGatewayContainer {

        bool Register<TEntityGateway>(TEntityGateway entityGateway) where TEntityGateway : IEntityGateway;

        TEntityGateway Get<TEntityGateway>() where TEntityGateway : IEntityGateway;

    }

}