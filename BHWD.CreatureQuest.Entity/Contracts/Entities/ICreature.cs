﻿namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface ICreature : IEntity {

        IAttack Attack { get; set; }
        ushort? AttackRating { get; set; }
        IColor Color { get; set; }
        ushort? DefenseRating { get; set; }
        IEvolution Evolution { get; set; }
        ushort? HealthPoints { get; set; }
        ILevel Level { get; set; }
        ushort? LuckRating { get; set; }
        ushort? ManaPool { get; set; }
        string Name { get; set; }
        ICreature NextCreature { get; set; }
        ushort? PowerRating { get; set; }
        ICreature PreviousCreature { get; set; }
        IRarity Rarity { get; set; }
        ISize Size { get; set; }

    }

}