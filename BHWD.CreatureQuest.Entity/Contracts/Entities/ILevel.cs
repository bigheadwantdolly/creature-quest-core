﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface ILevel : IEntity {

        List<ILevelRequirement> LevelRequirements { get; set; }
        int Number { get; set; }

    }

}