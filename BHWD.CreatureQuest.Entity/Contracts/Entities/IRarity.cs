﻿namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface IRarity : IEntity {

        string Name { get; set; }

    }

}