﻿namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface IColor : IEntity {

        string Name { get; set; }

    }

}