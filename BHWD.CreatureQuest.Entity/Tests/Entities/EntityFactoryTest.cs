﻿using System;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.Entity.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.Entity.Tests.Entities {

    [TestFixture]
    internal class EntityFactoryTest {

        private IEntityFactory EntityFactory { get; set; }

        [SetUp]
        public void SetUp() {
            this.EntityFactory = new EntityFactory();
            Entity.Entities.EntityFactory.Instance = this.EntityFactory;
        }

        [Test]
        public void Instances() {
            var actual = Entity.Entities.EntityFactory.Instance;
            var expected = this.EntityFactory;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Registers() {
            var actual = this.EntityFactory.Register<IDummy>(typeof(Dummy));
            const bool expected = true;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Gets() {
            this.EntityFactory.Register<IDummy>(typeof(Dummy));
            var actual = this.EntityFactory.Create<IDummy>();

            Assert.IsNotNull(actual);
        }

        [Test]
        public void TypesAreRetained() {
            this.EntityFactory.Register<IDummy>(typeof(Dummy));
            var actual = this.EntityFactory.Create<IDummy>().GetType();
            var expected = typeof(Dummy);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void SetsDefaultValues() {
            this.EntityFactory.Register<IDummy>(typeof(Dummy));
            var entity = this.EntityFactory.Create<IDummy>();
            const bool expected = true;
            var actual = entity.Id != Guid.Empty;

            Assert.That(actual == expected);
        }

    }

}