﻿using System;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;

namespace BHWD.CreatureQuest.Entity.Tests.TestDoubles {

    internal interface IDummy : IEntity {

    }

    internal class Dummy : BaseEntity, IDummy {

        public override void SetDefaults() {
            base.SetDefaults();

            this.Id = Guid.NewGuid();
        }

    }

}