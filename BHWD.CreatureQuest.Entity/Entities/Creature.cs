﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class Creature : BaseEntity, ICreature {

        public IAttack Attack { get; set; }
        public ushort? AttackRating { get; set; }
        public IColor Color { get; set; }
        public ushort? DefenseRating { get; set; }
        public IEvolution Evolution { get; set; }
        public ushort? HealthPoints { get; set; }
        public ILevel Level { get; set; }
        public ushort? LuckRating { get; set; }
        public ushort? ManaPool { get; set; }
        public string Name { get; set; }
        public ICreature NextCreature { get; set; }
        public ushort? PowerRating { get; set; }
        public ICreature PreviousCreature { get; set; }
        public IRarity Rarity { get; set; }
        public ISize Size { get; set; }

    }

}