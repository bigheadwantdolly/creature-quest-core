﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class Evolution : BaseEntity, IEvolution {

        public int Cardinality { get; set; }
        public List<IRarity> Rarities { get; set; }
        public List<ISize> Sizes { get; set; }

        public Evolution() {
            this.Rarities = new List<IRarity>();
            this.Sizes = new List<ISize>();
        }

    }

}