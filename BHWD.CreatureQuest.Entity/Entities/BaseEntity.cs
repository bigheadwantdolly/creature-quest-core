﻿using System;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class BaseEntity : IEntity {

        public Guid Id { get; set; }

        public virtual void SetDefaults() {
        }

    }

}