﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.Entity.EntityGateways {

    public class EntityGatewayContainer : IEntityGatewayContainer {

        public static IEntityGatewayContainer Instance { get; set; } = new EntityGatewayContainer();

        private Dictionary<Type, IEntityGateway> EntityGateways { get; } = new Dictionary<Type, IEntityGateway>();

        public bool Register<TEntityGateway>(TEntityGateway entityGateway) where TEntityGateway : IEntityGateway {
            var type = typeof(TEntityGateway);
            var registered = true;

            try {
                this.EntityGateways.Add(type, entityGateway);
            } catch (Exception exception) {
                Console.WriteLine(exception);
                registered = false;
            }

            return registered;
        }

        public TEntityGateway Get<TEntityGateway>() where TEntityGateway : IEntityGateway {
            var type = typeof(TEntityGateway);

            this.EntityGateways.TryGetValue(type, out var output);

            return (TEntityGateway) output;
        }

    }

}