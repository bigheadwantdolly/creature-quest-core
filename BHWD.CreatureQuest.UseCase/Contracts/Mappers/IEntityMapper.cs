﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.UseCase.Contracts.Mappers {

    public interface IEntityMapper<in TInput, TOutput> where TInput : IEntity {

        void MapOne(TInput input, TOutput output);
        void MapMany(IEnumerable<TInput> inputs, List<TOutput> outputs);

    }

}