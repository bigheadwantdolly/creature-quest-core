﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetLevelsOutput : IUseCaseOutput {

        List<Dictionary<string, object>> Levels { get; set; }

    }

}