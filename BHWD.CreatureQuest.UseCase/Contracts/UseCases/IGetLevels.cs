﻿namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetLevels : IUseCase<IGetLevelsInput, IGetLevelsOutput> {

    }

}