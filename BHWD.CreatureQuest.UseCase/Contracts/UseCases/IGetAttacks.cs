﻿namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetAttacks : IUseCase<IGetAttacksInput, IGetAttacksOutput> {

    }

}