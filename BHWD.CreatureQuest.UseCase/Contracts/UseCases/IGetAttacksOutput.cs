﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetAttacksOutput : IUseCaseOutput {

        List<Dictionary<string, object>> Attacks { get; set; }

    }

}