﻿namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IUseCaseContainer {

        bool Register<TUseCase, TUseCaseInput, TUseCaseOutput>(TUseCase useCase)
            where TUseCase : IUseCase<TUseCaseInput, TUseCaseOutput> where TUseCaseInput : IUseCaseInput where TUseCaseOutput : IUseCaseOutput;

        TUseCase Get<TUseCase, TUseCaseInput, TUseCaseOutput>()
            where TUseCase : IUseCase<TUseCaseInput, TUseCaseOutput> where TUseCaseInput : IUseCaseInput where TUseCaseOutput : IUseCaseOutput;

    }

}