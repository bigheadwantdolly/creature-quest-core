﻿using System;
using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetEvolutionsInput : IUseCaseInput {

        List<Guid> RarityIds { get; set; }
        List<Guid> SizeIds { get; set; }

    }

}