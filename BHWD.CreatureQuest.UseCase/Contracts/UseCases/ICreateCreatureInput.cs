﻿using System;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface ICreateCreatureInput : IUseCaseInput {

        Guid? AttackId { get; set; }
        ushort? AttackRating { get; set; }
        Guid? ColorId { get; set; }
        ushort? DefenseRating { get; set; }
        Guid? EvolutionId { get; set; }
        ushort? HealthPoints { get; set; }
        Guid? LevelId { get; set; }
        ushort? LuckRating { get; set; }
        ushort? ManaPool { get; set; }
        string Name { get; set; }
        Guid? NextCreatureId { get; set; }
        ushort? PowerRating { get; set; }
        Guid? PreviousCreatureId { get; set; }
        Guid? RarityId { get; set; }
        Guid? SizeId { get; set; }

    }

}