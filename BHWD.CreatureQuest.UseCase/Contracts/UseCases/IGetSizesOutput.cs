﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetSizesOutput : IUseCaseOutput {

        List<Dictionary<string, object>> Sizes { get; set; }

    }

}