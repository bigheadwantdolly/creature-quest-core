﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetEvolutionsOutput : IGetEvolutionsOutput {

        public List<Dictionary<string, object>> Evolutions { get; set; }

        public StubGetEvolutionsOutput() {
            this.Evolutions = new List<Dictionary<string, object>>();
        }

    }

}