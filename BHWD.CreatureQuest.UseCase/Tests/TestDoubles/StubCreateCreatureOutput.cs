﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubCreateCreatureOutput : ICreateCreatureOutput {

        public Dictionary<string, object> Creature { get; set; }

    }

}