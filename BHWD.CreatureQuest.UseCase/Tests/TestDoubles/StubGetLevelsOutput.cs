﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetLevelsOutput : IGetLevelsOutput {

        public List<Dictionary<string, object>> Levels { get; set; }

        public StubGetLevelsOutput() {
            this.Levels = new List<Dictionary<string, object>>();
        }

    }

}