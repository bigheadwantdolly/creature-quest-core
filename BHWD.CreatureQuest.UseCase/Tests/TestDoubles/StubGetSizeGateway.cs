﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetSizeGateway : IGetManyEntityGateway<ISize>, IGetOneEntityGateway<ISize> {

        private readonly List<ISize> Sizes;

        public StubGetSizeGateway(List<ISize> sizes) {
            this.Sizes = sizes;
        }

        public List<ISize> GetAll() {
            return this.Sizes;
        }

        public ISize GetById(Guid id) {
            return this.Sizes.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}