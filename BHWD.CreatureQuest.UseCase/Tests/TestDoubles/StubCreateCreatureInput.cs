﻿using System;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubCreateCreatureInput : ICreateCreatureInput {

        public Guid? AttackId { get; set; }
        public ushort? AttackRating { get; set; }
        public Guid? ColorId { get; set; }
        public ushort? DefenseRating { get; set; }
        public Guid? EvolutionId { get; set; }
        public ushort? HealthPoints { get; set; }
        public Guid? LevelId { get; set; }
        public ushort? LuckRating { get; set; }
        public ushort? ManaPool { get; set; }
        public string Name { get; set; }
        public Guid? NextCreatureId { get; set; }
        public ushort? PowerRating { get; set; }
        public Guid? PreviousCreatureId { get; set; }
        public Guid? RarityId { get; set; }
        public Guid? SizeId { get; set; }

    }

}