﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetManyCreatureGateway : IGetManyCreatureGateway {

        private readonly List<ICreature> Creatures;

        public StubGetManyCreatureGateway(List<ICreature> creatures) {
            this.Creatures = creatures;
        }

        public List<ICreature> GetAll() {
            return this.Creatures;
        }

        public List<ICreature> GetAll(List<Guid> colorIds, List<Guid> evolutionIds, List<Guid> sizeIds, Guid? levelId, string name) {
            return this.Creatures;
        }

    }

}