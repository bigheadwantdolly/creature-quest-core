﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetAttackGateway : IGetManyEntityGateway<IAttack>, IGetOneEntityGateway<IAttack> {

        private readonly List<IAttack> Attacks;

        public StubGetAttackGateway(List<IAttack> attacks) {
            this.Attacks = attacks;
        }

        public List<IAttack> GetAll() {
            return this.Attacks;
        }

        public IAttack GetById(Guid id) {
            return this.Attacks.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}