﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetSizesOutput : IGetSizesOutput {

        public List<Dictionary<string, object>> Sizes { get; set; }

        public StubGetSizesOutput() {
            this.Sizes = new List<Dictionary<string, object>>();
        }

    }

}