﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Mappers;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.Mappers {

    [TestFixture]
    internal class LevelRequirementMapperTest {

        private List<ILevelRequirement> LevelRequirements { get; set; }
        private IEntityMapper<ILevelRequirement, Dictionary<string, object>> EntityMapper { get; set; }

        [SetUp]
        public void SetUp() {
            this.LevelRequirements = new List<ILevelRequirement> {
                new LevelRequirement {
                    Id = Guid.NewGuid(),
                    Evolution = new Evolution {
                        Id = Guid.NewGuid(),
                        Cardinality = 1
                    },
                    Size = new Size {
                        Id = Guid.NewGuid(),
                        Name = "Mr Size"
                    }
                }
            };
            this.EntityMapper = new LevelRequirementEntityMapper(new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()), new SizeEntityMapper());
        }

        [Test]
        public void MapsId() {
            var output = new Dictionary<string, object>();
            var expected = this.LevelRequirements.FirstOrDefault()?.Id;

            this.EntityMapper.MapOne(this.LevelRequirements.FirstOrDefault(), output);

            output.TryGetValue(LevelRequirementEntityMapper.OutputKeyId, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsEvolution() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.LevelRequirements.FirstOrDefault(), output);

            output.TryGetValue(LevelRequirementEntityMapper.OutputKeyEvolution, out var actual);

            Assert.NotNull(actual);
        }

        [Test]
        public void MapsSize() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.LevelRequirements.FirstOrDefault(), output);

            output.TryGetValue(LevelRequirementEntityMapper.OutputKeySize, out var actual);

            Assert.NotNull(actual);
        }

    }

}