﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Mappers;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.Mappers {

    [TestFixture]
    internal class AttackMapperTest {

        private List<IAttack> Attacks { get; set; }
        private IEntityMapper<IAttack, Dictionary<string, object>> EntityMapper { get; set; }

        [SetUp]
        public void SetUp() {
            this.Attacks = new List<IAttack> {
                new Attack {
                    Id = Guid.NewGuid(),
                    Name = "Test Attack"
                }
            };
            this.EntityMapper = new AttackEntityMapper();
        }

        [Test]
        public void MapsId() {
            var output = new Dictionary<string, object>();
            var expected = this.Attacks.FirstOrDefault()?.Id;

            this.EntityMapper.MapOne(this.Attacks.FirstOrDefault(), output);

            output.TryGetValue(AttackEntityMapper.OutputKeyId, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsName() {
            var output = new Dictionary<string, object>();
            var expected = this.Attacks.FirstOrDefault()?.Name;

            this.EntityMapper.MapOne(this.Attacks.FirstOrDefault(), output);

            output.TryGetValue(AttackEntityMapper.OutputKeyName, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}