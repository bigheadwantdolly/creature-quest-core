﻿using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Mappers;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.Mappers {

    [TestFixture]
    internal class SizeMapperTest {

        private List<ISize> Sizes { get; set; }
        private IEntityMapper<ISize, Dictionary<string, object>> EntityMapper { get; set; }

        [SetUp]
        public void SetUp() {
            this.Sizes = new List<ISize> {
                new Size {
                    Name = "Test Size"
                }
            };
            this.EntityMapper = new SizeEntityMapper();
        }

        [Test]
        public void MapsName() {
            var output = new Dictionary<string, object>();
            var expected = this.Sizes.FirstOrDefault()?.Name;

            this.EntityMapper.MapOne(this.Sizes.FirstOrDefault(), output);

            output.TryGetValue(SizeEntityMapper.OutputKeyName, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}