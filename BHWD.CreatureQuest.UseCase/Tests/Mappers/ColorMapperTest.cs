﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Mappers;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.Mappers {

    [TestFixture]
    internal class ColorMapperTest {

        private List<IColor> Colors { get; set; }
        private IEntityMapper<IColor, Dictionary<string, object>> EntityMapper { get; set; }

        [SetUp]
        public void SetUp() {
            this.Colors = new List<IColor> {
                new Color {
                    Id = Guid.NewGuid(),
                    Name = "Test Color"
                }
            };
            this.EntityMapper = new ColorEntityMapper();
        }

        [Test]
        public void MapsId() {
            var output = new Dictionary<string, object>();
            var expected = this.Colors.FirstOrDefault()?.Id;

            this.EntityMapper.MapOne(this.Colors.FirstOrDefault(), output);

            output.TryGetValue(ColorEntityMapper.OutputKeyId, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsName() {
            var output = new Dictionary<string, object>();
            var expected = this.Colors.FirstOrDefault()?.Name;

            this.EntityMapper.MapOne(this.Colors.FirstOrDefault(), output);

            output.TryGetValue(ColorEntityMapper.OutputKeyName, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}