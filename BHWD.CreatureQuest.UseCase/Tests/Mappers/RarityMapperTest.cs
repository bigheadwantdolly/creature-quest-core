﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Mappers;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.Mappers {

    [TestFixture]
    internal class RarityMapperTest {

        private List<IRarity> Rarities { get; set; }
        private IEntityMapper<IRarity, Dictionary<string, object>> EntityMapper { get; set; }

        [SetUp]
        public void SetUp() {
            this.Rarities = new List<IRarity> {
                new Rarity {
                    Id = Guid.NewGuid(),
                    Name = "Test Rarity"
                }
            };
            this.EntityMapper = new RarityEntityMapper();
        }

        [Test]
        public void MapsId() {
            var output = new Dictionary<string, object>();
            var expected = this.Rarities.FirstOrDefault()?.Id;

            this.EntityMapper.MapOne(this.Rarities.FirstOrDefault(), output);

            output.TryGetValue(RarityEntityMapper.OutputKeyId, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsName() {
            var output = new Dictionary<string, object>();
            var expected = this.Rarities.FirstOrDefault()?.Name;

            this.EntityMapper.MapOne(this.Rarities.FirstOrDefault(), output);

            output.TryGetValue(RarityEntityMapper.OutputKeyName, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}