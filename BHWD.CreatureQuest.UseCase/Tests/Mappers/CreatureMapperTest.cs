﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Mappers;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.Mappers {

    [TestFixture]
    internal class CreatureMapperTest {

        private List<ICreature> Creatures { get; set; }
        private IEntityMapper<ICreature, Dictionary<string, object>> EntityMapper { get; set; }

        [SetUp]
        public void SetUp() {
            this.Creatures = new List<ICreature> {
                new Creature {
                    Attack = new Attack {
                        Name = "attack"
                    },
                    AttackRating = 1,
                    Id = Guid.NewGuid(),
                    Color = new Color {
                        Name = "color"
                    },
                    DefenseRating = 2,
                    Evolution = new Evolution {
                        Cardinality = 1
                    },
                    HealthPoints = 3,
                    Level = new Level {
                        Number = 15
                    },
                    LuckRating = 4,
                    ManaPool = 5,
                    Name = "Test Creature",
                    NextCreature = new Creature {
                        Name = "Mr. Creature"
                    },
                    PowerRating = 6,
                    PreviousCreature = new Creature {
                        Name = "Mr. Creature"
                    },
                    Rarity = new Rarity {
                        Name = "rarity"
                    },
                    Size = new Size {
                        Name = "size"
                    }
                }
            };
            this.EntityMapper = new CreatureEntityMapper(
                new AttackEntityMapper(),
                new ColorEntityMapper(),
                new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()),
                new LevelEntityMapper(new LevelRequirementEntityMapper(new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()), new SizeEntityMapper())),
                new RarityEntityMapper(),
                new SizeEntityMapper());
        }

        [Test]
        public void MapsAttack() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyAttack, out var actual);
            var validResult = true;

            if (!(actual is Dictionary<string, object> actualDictionary) || actualDictionary.Count == 0) {
                validResult = false;
            }

            Assert.IsTrue(validResult);
        }

        [Test]
        public void MapsAttackRating() {
            var output = new Dictionary<string, object>();
            var expected = this.Creatures.FirstOrDefault()?.AttackRating;

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyAttackRating, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsDefenseRating() {
            var output = new Dictionary<string, object>();
            var expected = this.Creatures.FirstOrDefault()?.DefenseRating;

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyDefenseRating, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsHealthPoints() {
            var output = new Dictionary<string, object>();
            var expected = this.Creatures.FirstOrDefault()?.HealthPoints;

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyHealthPoints, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsLuckRating() {
            var output = new Dictionary<string, object>();
            var expected = this.Creatures.FirstOrDefault()?.LuckRating;

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyLuckRating, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsManaPool() {
            var output = new Dictionary<string, object>();
            var expected = this.Creatures.FirstOrDefault()?.ManaPool;

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyManaPool, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsNextCreature() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyNextCreature, out var actual);
            var validResult = true;

            if (!(actual is Dictionary<string, object> actualDictionary) || actualDictionary.Count == 0) {
                validResult = false;
            }

            Assert.IsTrue(validResult);
        }

        [Test]
        public void MapsPowerRating() {
            var output = new Dictionary<string, object>();
            var expected = this.Creatures.FirstOrDefault()?.PowerRating;

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyPowerRating, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsPreviousCreature() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyPreviousCreature, out var actual);
            var validResult = true;

            if (!(actual is Dictionary<string, object> actualDictionary) || actualDictionary.Count == 0) {
                validResult = false;
            }

            Assert.IsTrue(validResult);
        }

        [Test]
        public void MapsId() {
            var output = new Dictionary<string, object>();
            var expected = this.Creatures.FirstOrDefault()?.Id;

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyId, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsColor() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyColor, out var actual);
            var validResult = true;

            if (!(actual is Dictionary<string, object> actualDictionary) || actualDictionary.Count == 0) {
                validResult = false;
            }

            Assert.IsTrue(validResult);
        }

        [Test]
        public void MapsEvolution() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyEvolution, out var actual);
            var validResult = true;

            if (!(actual is Dictionary<string, object> actualDictionary) || actualDictionary.Count == 0) {
                validResult = false;
            }

            Assert.IsTrue(validResult);
        }

        [Test]
        public void MapsLevel() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyLevel, out var actual);
            var validResult = true;

            if (!(actual is Dictionary<string, object> actualDictionary) || actualDictionary.Count == 0) {
                validResult = false;
            }

            Assert.IsTrue(validResult);
        }

        [Test]
        public void MapsName() {
            var output = new Dictionary<string, object>();
            var expected = this.Creatures.FirstOrDefault()?.Name;

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyName, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsRarity() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeyRarity, out var actual);
            var validResult = true;

            if (!(actual is Dictionary<string, object> actualDictionary) || actualDictionary.Count == 0) {
                validResult = false;
            }

            Assert.IsTrue(validResult);
        }

        [Test]
        public void MapsSize() {
            var output = new Dictionary<string, object>();

            this.EntityMapper.MapOne(this.Creatures.FirstOrDefault(), output);

            output.TryGetValue(CreatureEntityMapper.OutputKeySize, out var actual);
            var validResult = true;

            if (!(actual is Dictionary<string, object> actualDictionary) || actualDictionary.Count == 0) {
                validResult = false;
            }

            Assert.IsTrue(validResult);
        }

        [Test]
        public void MapsMany() {
            var output = new List<Dictionary<string, object>>();
            var expected = this.Creatures.Count;

            this.EntityMapper.MapMany(this.Creatures, output);

            var actual = output.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}