﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.Mappers;
using BHWD.CreatureQuest.UseCase.Tests.TestDoubles;
using BHWD.CreatureQuest.UseCase.UseCases;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.UseCases {

    [TestFixture]
    internal class GetLevelsTest {

        private IGetLevels GetLevels { get; set; }
        private List<ILevel> Levels { get; set; }
        private List<IEvolution> Evolutions { get; set; }
        private ISize Size { get; set; }

        [SetUp]
        public void SetUp() {
            var levelMapper = new LevelEntityMapper(new LevelRequirementEntityMapper(new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()), new SizeEntityMapper()));
            this.Evolutions = new List<IEvolution> {
                new Evolution {
                    Id = Guid.NewGuid(),
                    Cardinality = 1
                },
                new Evolution {
                    Id = Guid.NewGuid(),
                    Cardinality = 6
                }
            };
            this.Size = new Size {
                Id = Guid.NewGuid(),
                Name = "Mr Size"
            };

            this.Levels = new List<ILevel> {
                new Level {
                    Id = Guid.NewGuid(),
                    Number = 15,
                    LevelRequirements = new List<ILevelRequirement> {
                        new LevelRequirement {
                            Id = Guid.NewGuid(),
                            Evolution = this.Evolutions[0]
                        }
                    }
                },
                new Level {
                    Id = Guid.NewGuid(),
                    Number = 30,
                    LevelRequirements = new List<ILevelRequirement> {
                        new LevelRequirement {
                            Id = Guid.NewGuid(),
                            Evolution = this.Evolutions[1]
                        }
                    }
                },
                new Level {
                    Id = Guid.NewGuid(),
                    Number = 45,
                    LevelRequirements = new List<ILevelRequirement> {
                        new LevelRequirement {
                            Id = Guid.NewGuid(),
                            Evolution = this.Evolutions[1],
                            Size = this.Size
                        }
                    }
                }
            };
            this.GetLevels = new GetLevels(new StubGetLevelGateway(this.Levels), levelMapper);
        }

        [Test]
        public void OutputsEntitiesWithoutFilter() {
            var useCaseInput = new StubGetLevelsInput();
            var useCaseOutput = new StubGetLevelsOutput();

            this.GetLevels.Execute(useCaseInput, useCaseOutput);
            var actual = useCaseOutput.Levels.Count;
            var exepcted = this.Levels.Count;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

        [Test]
        public void OutputsEntitiesWithEvolutionFilter() {
            var useCaseInput = new StubGetLevelsInput();
            var useCaseOutput = new StubGetLevelsOutput();

            useCaseInput.EvolutionId = this.Evolutions[0].Id;

            this.GetLevels.Execute(useCaseInput, useCaseOutput);
            var actual = useCaseOutput.Levels.Count;
            const int exepcted = 1;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

        [Test]
        public void OutputsEntitiesWithOnlyAnEvolutionRequirement() {
            var useCaseInput = new StubGetLevelsInput();
            var useCaseOutput = new StubGetLevelsOutput();

            useCaseInput.EvolutionId = this.Evolutions[0].Id;
            useCaseInput.SizeId = this.Size.Id;

            this.GetLevels.Execute(useCaseInput, useCaseOutput);
            var actual = useCaseOutput.Levels.Count;
            const int exepcted = 1;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

        [Test]
        public void OutputsEntitiesWithEvolutionAndSizeFilter() {
            var useCaseInput = new StubGetLevelsInput();
            var useCaseOutput = new StubGetLevelsOutput();

            useCaseInput.EvolutionId = this.Evolutions[1].Id;
            useCaseInput.SizeId = this.Size.Id;

            this.GetLevels.Execute(useCaseInput, useCaseOutput);
            var actual = useCaseOutput.Levels.Count;
            const int exepcted = 2;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

        [Test]
        public void IgnoresSizeFilterIfNoEvolutionIsPresesnt() {
            var useCaseInput = new StubGetLevelsInput();
            var useCaseOutput = new StubGetLevelsOutput();

            useCaseInput.SizeId = this.Size.Id;

            this.GetLevels.Execute(useCaseInput, useCaseOutput);
            var actual = useCaseOutput.Levels.Count;
            var exepcted = this.Levels.Count;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

    }

}