﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.Mappers;
using BHWD.CreatureQuest.UseCase.Tests.TestDoubles;
using BHWD.CreatureQuest.UseCase.UseCases;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.UseCases {

    [TestFixture]
    internal class GetRaritiesTest {

        private IGetRarities GetRarities { get; set; }
        private List<IRarity> Rarities { get; set; }
        private IGetRaritiesInput GetRaritiesInput { get; set; }
        private IGetRaritiesOutput GetRaritiesOutput { get; set; }

        [SetUp]
        public void SetUp() {
            var rarityMapper = new RarityEntityMapper();

            this.Rarities = new List<IRarity> {
                new Rarity(),
                new Rarity(),
                new Rarity()
            };
            this.GetRarities = new GetRarities(new StubGetRarityGateway(this.Rarities), rarityMapper);
            this.GetRaritiesInput = new StubGetRaritiesInput();
            this.GetRaritiesOutput = new StubGetRaritiesOutput();

            this.GetRarities.Execute(this.GetRaritiesInput, this.GetRaritiesOutput);
        }

        [Test]
        public void OutputsRarities() {
            var actual = this.GetRaritiesOutput.Rarities.Count;
            var exepcted = this.Rarities.Count;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

    }

}