﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.Mappers;
using BHWD.CreatureQuest.UseCase.Tests.TestDoubles;
using BHWD.CreatureQuest.UseCase.UseCases;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.UseCases {

    [TestFixture]
    internal class CreateCreatureTest {

        private ICreateCreature UseCase { get; set; }
        private ICreateCreatureInput UseCaseInput { get; set; }
        private readonly List<IAttack> Attacks = new List<IAttack>();
        private readonly List<IColor> Colors = new List<IColor>();
        private readonly List<ICreature> Creatures = new List<ICreature>();
        private readonly List<IEvolution> Evolutions = new List<IEvolution>();
        private readonly List<ILevel> Levels = new List<ILevel>();
        private readonly List<ISize> Sizes = new List<ISize>();
        private readonly List<IRarity> Rarities = new List<IRarity>();
        private readonly ICreature testCreature = new Creature
        {
            Id = Guid.NewGuid(),
            Name = "Var-char-izord"
        };
        private IEntityFactory EntityFactory { get; set; }

        [SetUp]
        public void SetUp() {
            this.Attacks.Add(
                new Attack {
                    Id = Guid.NewGuid(),
                    Name = "Mr Attack"
                });
            this.Colors.Add(
                new Color {
                    Id = Guid.NewGuid(),
                    Name = "Mr Color"
                });
            this.Creatures.Add(
                new Creature {
                    Id = Guid.NewGuid(),
                    Name = "Junior Mint"
                });
            this.Evolutions.Add(
                new Evolution {
                    Id = Guid.NewGuid(),
                    Cardinality = 1
                });
            this.Levels.Add(
                new Level {
                    Id = Guid.NewGuid(),
                    Number = 15
                });
            this.Sizes.Add(
                new Size {
                    Id = Guid.NewGuid(),
                    Name = "Mr Size"
                });
            this.Rarities.Add(
                new Rarity {
                    Id = Guid.NewGuid(),
                    Name = "Mr Rarity"
                });

            this.EntityFactory = new EntityFactory();
            this.EntityFactory.Register<ICreature>(typeof(Creature));

            this.UseCase = new CreateCreature(
                new StubCreateCreatureGateway(),
                new CreatureEntityMapper(
                    new AttackEntityMapper(),
                    new ColorEntityMapper(),
                    new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()),
                    new LevelEntityMapper(new LevelRequirementEntityMapper(new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()), new SizeEntityMapper())),
                    new RarityEntityMapper(),
                    new SizeEntityMapper()),
                this.EntityFactory,
                new StubGetAttackGateway(this.Attacks),
                new StubGetColorGateway(this.Colors),
                new StubGetCreatureGateway(this.Creatures),
                new StubGetEvolutionGateway(this.Evolutions),
                new StubGetLevelGateway(this.Levels),
                new StubGetRarityGateway(this.Rarities),
                new StubGetSizeGateway(this.Sizes),
                new StubGetManyCreatureGateway(this.Creatures));

            this.UseCaseInput = new StubCreateCreatureInput {
                AttackId = this.Attacks[0].Id,
                AttackRating = 1,
                ColorId = this.Colors[0].Id,
                DefenseRating = 2,
                EvolutionId = this.Evolutions[0].Id,
                HealthPoints = 3,
                LevelId = this.Levels[0].Id,
                LuckRating = 4,
                ManaPool = 5,
                Name = "Mr Creature",
                NextCreatureId = this.Creatures[0].Id,
                PowerRating = 6,
                PreviousCreatureId = this.Creatures[0].Id,
                RarityId = this.Rarities[0].Id,
                SizeId = this.Sizes[0].Id
            };
        }

        [Test]
        public void AppendsIdToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyId, out var expected);

            Assert.That(expected, Is.Not.Null);
        }

        [Test]
        public void AppendsAttackToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyAttack, out var expected);

            Assert.That(expected, Is.Not.Empty);
        }

        [Test]
        public void AppendsAttackPowerToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyAttackRating, out var expected);

            Assert.That(expected, Is.Not.Null);
        }

        [Test]
        public void AppendsColorToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyColor, out var expected);

            Assert.That(expected, Is.Not.Empty);
        }

        [Test]
        public void AppendsDefensePowerToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyDefenseRating, out var expected);

            Assert.That(expected, Is.Not.Null);
        }

        [Test]
        public void AppendsEvolutionToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyEvolution, out var expected);

            Assert.That(expected, Is.Not.Empty);
        }

        [Test]
        public void AppendsHealthPointsToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyHealthPoints, out var expected);

            Assert.That(expected, Is.Not.Null);
        }

        [Test]
        public void AppendsLevelToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyLevel, out var expected);

            Assert.That(expected, Is.Not.Null);
        }

        [Test]
        public void AppendsLuckRatingToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyLuckRating, out var expected);

            Assert.That(expected, Is.Not.Null);
        }

        [Test]
        public void AppendsManaPoolToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyManaPool, out var expected);

            Assert.That(expected, Is.Not.Null);
        }

        [Test]
        public void AppendsNameToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyName, out var expected);

            Assert.That(expected, Is.Not.Null);
        }

        [Test]
        public void AppendsNextCreatureToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyNextCreature, out var expected);

            Assert.That(expected, Is.Not.Empty);
        }

        [Test]
        public void AppendsPowerRatingToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyPowerRating, out var expected);

            Assert.That(expected, Is.Not.Null);
        }

        [Test]
        public void AppendsPreviousCreatureToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyPreviousCreature, out var expected);

            Assert.That(expected, Is.Not.Empty);
        }

        [Test]
        public void AppendsRarityToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeyRarity, out var expected);

            Assert.That(expected, Is.Not.Empty);
        }

        [Test]
        public void AppendsSizeToRecord() {
            var useCaseOutput = new StubCreateCreatureOutput();

            this.UseCase.Execute(this.UseCaseInput, useCaseOutput);
            useCaseOutput.Creature.TryGetValue(CreatureEntityMapper.OutputKeySize, out var expected);

            Assert.That(expected, Is.Not.Empty);
        }

        [Test]
        public void ThrowsExceptionIfAttackIsMissing() {
            var test = new Action(
                () => {
                    var useCaseInput = new StubCreateCreatureInput {
                        ColorId = this.UseCaseInput.ColorId,
                        EvolutionId = this.UseCaseInput.EvolutionId,
                        LevelId = this.UseCaseInput.LevelId,
                        Name = this.UseCaseInput.Name,
                        RarityId = this.UseCaseInput.RarityId,
                        SizeId = this.UseCaseInput.SizeId
                    };
                    var useCaseOutput = new StubCreateCreatureOutput();

                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });

            Assert.Throws<InvalidOperationException>(() => test());
        }

        [Test]
        public void ThrowsExceptionIfColorIsMissing() {
            var test = new Action(
                () => {
                    var useCaseInput = new StubCreateCreatureInput {
                        AttackId = this.UseCaseInput.AttackId,
                        EvolutionId = this.UseCaseInput.EvolutionId,
                        LevelId = this.UseCaseInput.LevelId,
                        Name = this.UseCaseInput.Name,
                        RarityId = this.UseCaseInput.RarityId,
                        SizeId = this.UseCaseInput.SizeId
                    };
                    var useCaseOutput = new StubCreateCreatureOutput();

                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });

            Assert.Throws<InvalidOperationException>(() => test());
        }

        [Test]
        public void ThrowsExceptionIfEvolutionIsMissing() {
            var test = new Action(
                () => {
                    var useCaseInput = new StubCreateCreatureInput {
                        AttackId = this.UseCaseInput.AttackId,
                        ColorId = this.UseCaseInput.ColorId,
                        LevelId = this.UseCaseInput.LevelId,
                        Name = this.UseCaseInput.Name,
                        RarityId = this.UseCaseInput.RarityId,
                        SizeId = this.UseCaseInput.SizeId
                    };
                    var useCaseOutput = new StubCreateCreatureOutput();

                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });

            Assert.Throws<InvalidOperationException>(() => test());
        }

        [Test]
        public void ThrowsExceptionIfLevelIsMissing() {
            var test = new Action(
                () => {
                    var useCaseInput = new StubCreateCreatureInput {
                        AttackId = this.UseCaseInput.AttackId,
                        ColorId = this.UseCaseInput.ColorId,
                        EvolutionId = this.UseCaseInput.EvolutionId,
                        Name = this.UseCaseInput.Name,
                        RarityId = this.UseCaseInput.RarityId,
                        SizeId = this.UseCaseInput.SizeId
                    };
                    var useCaseOutput = new StubCreateCreatureOutput();

                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });

            Assert.Throws<InvalidOperationException>(() => test());
        }

        [Test]
        public void ThrowsExceptionIfNameIsMissing() {
            var test = new Action(
                () => {
                    var useCaseInput = new StubCreateCreatureInput {
                        AttackId = this.UseCaseInput.AttackId,
                        ColorId = this.UseCaseInput.ColorId,
                        EvolutionId = this.UseCaseInput.EvolutionId,
                        LevelId = this.UseCaseInput.LevelId,
                        RarityId = this.UseCaseInput.RarityId,
                        SizeId = this.UseCaseInput.SizeId
                    };
                    var useCaseOutput = new StubCreateCreatureOutput();

                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });

            Assert.Throws<InvalidOperationException>(() => test());
        }

        [Test]
        public void ThrowsExceptionIfRarityIsMissing() {
            var test = new Action(
                () => {
                    var useCaseInput = new StubCreateCreatureInput {
                        AttackId = this.UseCaseInput.AttackId,
                        ColorId = this.UseCaseInput.ColorId,
                        EvolutionId = this.UseCaseInput.EvolutionId,
                        LevelId = this.UseCaseInput.LevelId,
                        Name = this.UseCaseInput.Name,
                        SizeId = this.UseCaseInput.SizeId
                    };
                    var useCaseOutput = new StubCreateCreatureOutput();

                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });

            Assert.Throws<InvalidOperationException>(() => test());
        }

        [Test]
        public void ThrowsExceptionIfSizeIsMissing() {
            var test = new Action(
                () => {
                    var useCaseInput = new StubCreateCreatureInput {
                        AttackId = this.UseCaseInput.AttackId,
                        ColorId = this.UseCaseInput.ColorId,
                        EvolutionId = this.UseCaseInput.EvolutionId,
                        LevelId = this.UseCaseInput.LevelId,
                        Name = this.UseCaseInput.Name,
                        RarityId = this.UseCaseInput.RarityId
                    };
                    var useCaseOutput = new StubCreateCreatureOutput();

                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });

            Assert.Throws<InvalidOperationException>(() => test());
        }
    
        [Test]
        public void ThrowsExceptionIfIsDuplicatePreviousCreature()
        {
            UseCaseInput.PreviousCreatureId = testCreature.Id;
            this.Creatures[0].PreviousCreature = testCreature;
            this.Creatures.Add(testCreature);
            var test = new Action(
                () => {
                    var useCaseInput = this.UseCaseInput;
                    var useCaseOutput = new StubCreateCreatureOutput();
    
                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });
    
        Assert.Throws<InvalidOperationException>(() => test());
        }
        
        [Test]
        public void ThrowsExceptionIfIsDuplicatePreviousCreatureAndDifferentName()
        {

            UseCaseInput.PreviousCreatureId = testCreature.Id;
            this.Creatures[0].PreviousCreature = testCreature;
            this.Creatures.Add(testCreature);
            UseCaseInput.Name = "Wowie Mandell";
            var test = new Action(
                () => {
                    var useCaseInput = this.UseCaseInput;
                    var useCaseOutput = new StubCreateCreatureOutput();
    
                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });
    
            Assert.Throws<InvalidOperationException>(() => test());
        }
        
        [Test]
        public void ThrowsExceptionIfIsDuplicateNextCreature()
        {
            this.Creatures[0].NextCreature = testCreature;
            this.Creatures.Add(testCreature);
            UseCaseInput.NextCreatureId = testCreature.Id;

            var test = new Action(
                () => {
                    var useCaseInput = this.UseCaseInput;
                    var useCaseOutput = new StubCreateCreatureOutput();
    
                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });
    
            Assert.Throws<InvalidOperationException>(() => test());
        }
        
        [Test]
        public void ThrowsExceptionIfIsDuplicateNextCreatureAndDifferentName()
        {
            UseCaseInput.NextCreatureId = testCreature.Id;
            this.Creatures[0].NextCreature = testCreature;
            this.Creatures.Add(testCreature);
            UseCaseInput.Name = "Geronimo";
            var test = new Action(
                () => {
                    var useCaseInput = this.UseCaseInput;
                    var useCaseOutput = new StubCreateCreatureOutput();
    
                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });
    
            Assert.Throws<InvalidOperationException>(() => test());
        }
        
        [Test]
        public void ThrowsExceptionIfIsDuplicateName()
        {

            UseCaseInput.Name = "Junior Mint";
            var test = new Action(
                () => {
                    var useCaseInput = this.UseCaseInput;
                    var useCaseOutput = new StubCreateCreatureOutput();
    
                    this.UseCase.Execute(useCaseInput, useCaseOutput);
                });
    
            Assert.Throws<InvalidOperationException>(() => test());
        }

    }

}