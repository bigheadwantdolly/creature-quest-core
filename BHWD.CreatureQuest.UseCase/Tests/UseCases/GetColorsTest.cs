﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.Mappers;
using BHWD.CreatureQuest.UseCase.Tests.TestDoubles;
using BHWD.CreatureQuest.UseCase.UseCases;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.UseCases {

    [TestFixture]
    internal class GetColorsTest {

        private IGetColors GetColors { get; set; }
        private List<IColor> Colors { get; set; }
        private IGetColorsInput GetColorsInput { get; set; }
        private IGetColorsOutput GetColorsOutput { get; set; }

        [SetUp]
        public void SetUp() {
            var colorMapper = new ColorEntityMapper();

            this.Colors = new List<IColor> {
                new Color(),
                new Color(),
                new Color()
            };
            this.GetColors = new GetColors(new StubGetColorGateway(this.Colors), colorMapper);
            this.GetColorsInput = new StubGetColorsInput();
            this.GetColorsOutput = new StubGetColorsOutput();

            this.GetColors.Execute(this.GetColorsInput, this.GetColorsOutput);
        }

        [Test]
        public void OutputsColors() {
            var actual = this.GetColorsOutput.Colors.Count;
            var exepcted = this.Colors.Count;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

    }

}