﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;

namespace BHWD.CreatureQuest.UseCase.Mappers {

    public class LevelEntityMapper : BaseEntityMapper<ILevel, Dictionary<string, object>> {

        private readonly IEntityMapper<ILevelRequirement, Dictionary<string, object>> LevelRequirementEntityMapper;

        public const string OutputKeyLevelRequirements = "levelRequirements";
        public const string OutputKeyNumber = "number";

        public LevelEntityMapper(IEntityMapper<ILevelRequirement, Dictionary<string, object>> levelRequirementEntityMapper) {
            this.LevelRequirementEntityMapper = levelRequirementEntityMapper;
        }

        public override void MapOne(ILevel input, Dictionary<string, object> output) {
            output.Add(OutputKeyId, input.Id);
            output.Add(OutputKeyNumber, input.Number);

            this.MapLevelRequirements(input, output);
        }

        private void MapLevelRequirements(ILevel input, IDictionary<string, object> output) {
            var levelRequirements = new List<Dictionary<string, object>>();

            if (input.LevelRequirements != null) {
                this.LevelRequirementEntityMapper.MapMany(input.LevelRequirements, levelRequirements);
            }

            output.Add(OutputKeyLevelRequirements, levelRequirements);
        }

    }

}