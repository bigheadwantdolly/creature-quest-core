﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;

namespace BHWD.CreatureQuest.UseCase.Mappers {

    public class LevelRequirementEntityMapper : BaseEntityMapper<ILevelRequirement, Dictionary<string, object>> {

        public const string OutputKeyEvolution = "evolution";
        public const string OutputKeySize = "size";

        private readonly IEntityMapper<IEvolution, Dictionary<string, object>> EvolutionEntityMapper;
        private readonly IEntityMapper<ISize, Dictionary<string, object>> SizeEntityMapper;

        public LevelRequirementEntityMapper(IEntityMapper<IEvolution, Dictionary<string, object>> evolutionEntityMapper, IEntityMapper<ISize, Dictionary<string, object>> sizeEntityMapper) {
            this.EvolutionEntityMapper = evolutionEntityMapper;
            this.SizeEntityMapper = sizeEntityMapper;
        }

        public override void MapOne(ILevelRequirement input, Dictionary<string, object> output) {
            output.Add(OutputKeyId, input.Id);

            this.MapEvolution(input, output);
            this.MapSize(input, output);
        }

        private void MapEvolution(ILevelRequirement input, IDictionary<string, object> output) {
            var evolution = new Dictionary<string, object>();

            if (input.Evolution != null) {
                this.EvolutionEntityMapper.MapOne(input.Evolution, evolution);
            }

            output.Add(OutputKeyEvolution, evolution);
        }

        private void MapSize(ILevelRequirement input, IDictionary<string, object> output) {
            var size = new Dictionary<string, object>();

            if (input.Size != null) {
                this.SizeEntityMapper.MapOne(input.Size, size);
            }

            output.Add(OutputKeySize, size);
        }

    }

}