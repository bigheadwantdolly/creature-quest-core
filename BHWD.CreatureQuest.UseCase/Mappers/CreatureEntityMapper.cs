﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;

namespace BHWD.CreatureQuest.UseCase.Mappers {

    public class CreatureEntityMapper : BaseEntityMapper<ICreature, Dictionary<string, object>> {

        public const string OutputKeyAttack = "attack";
        public const string OutputKeyAttackRating = "attackRating";
        public const string OutputKeyColor = "color";
        public const string OutputKeyDefenseRating = "defenseRating";
        public const string OutputKeyEvolution = "evolution";
        public const string OutputKeyHealthPoints = "healthPoints";
        public const string OutputKeyLevel = "level";
        public const string OutputKeyLuckRating = "luckRating";
        public const string OutputKeyManaPool = "manaPool";
        public const string OutputKeyName = "name";
        public const string OutputKeyNextCreature = "nextCreature";
        public const string OutputKeyPowerRating = "powerRating";
        public const string OutputKeyPreviousCreature = "previousCreature";
        public const string OutputKeyRarity = "rarity";
        public const string OutputKeySize = "size";

        private readonly IEntityMapper<IAttack, Dictionary<string, object>> AttackEntityMapper;
        private readonly IEntityMapper<IColor, Dictionary<string, object>> ColorEntityMapper;
        private readonly IEntityMapper<IEvolution, Dictionary<string, object>> EvolutionEntityMapper;
        private readonly IEntityMapper<ILevel, Dictionary<string, object>> LevelEntityMapper;
        private readonly IEntityMapper<IRarity, Dictionary<string, object>> RarityEntityMapper;
        private readonly IEntityMapper<ISize, Dictionary<string, object>> SizeEntityMapper;

        public CreatureEntityMapper(
            IEntityMapper<IAttack, Dictionary<string, object>> attackEntityMapper,
            IEntityMapper<IColor, Dictionary<string, object>> colorEntityMapper,
            IEntityMapper<IEvolution, Dictionary<string, object>> evolutionEntityMapper,
            IEntityMapper<ILevel, Dictionary<string, object>> levelEntityMapper,
            IEntityMapper<IRarity, Dictionary<string, object>> rarityEntityMapper,
            IEntityMapper<ISize, Dictionary<string, object>> sizeEntityMapper) {
            this.AttackEntityMapper = attackEntityMapper;
            this.ColorEntityMapper = colorEntityMapper;
            this.EvolutionEntityMapper = evolutionEntityMapper;
            this.LevelEntityMapper = levelEntityMapper;
            this.RarityEntityMapper = rarityEntityMapper;
            this.SizeEntityMapper = sizeEntityMapper;
        }

        public override void MapOne(ICreature input, Dictionary<string, object> output) {
            output.Add(OutputKeyId, input.Id);
            output.Add(OutputKeyAttackRating, input.AttackRating);
            output.Add(OutputKeyDefenseRating, input.DefenseRating);
            output.Add(OutputKeyHealthPoints, input.HealthPoints);
            output.Add(OutputKeyLuckRating, input.LuckRating);
            output.Add(OutputKeyManaPool, input.ManaPool);
            output.Add(OutputKeyName, input.Name);
            output.Add(OutputKeyPowerRating, input.PowerRating);

            this.MapAttack(input, output);
            this.MapColor(input, output);
            this.MapEvolution(input, output);
            this.MapLevel(input, output);
            this.MapNextCreature(input, output);
            this.MapPreviousCreature(input, output);
            this.MapRarity(input, output);
            this.MapSize(input, output);
        }

        private void MapAttack(ICreature input, IDictionary<string, object> output) {
            var attack = new Dictionary<string, object>();

            if (input.Attack != null) {
                this.AttackEntityMapper.MapOne(input.Attack, attack);
            }

            output.Add(OutputKeyAttack, attack);
        }

        private void MapColor(ICreature input, IDictionary<string, object> output) {
            var color = new Dictionary<string, object>();

            if (input.Color != null) {
                this.ColorEntityMapper.MapOne(input.Color, color);
            }

            output.Add(OutputKeyColor, color);
        }

        private void MapEvolution(ICreature input, IDictionary<string, object> output) {
            var evolution = new Dictionary<string, object>();

            if (input.Evolution != null) {
                this.EvolutionEntityMapper.MapOne(input.Evolution, evolution);
            }

            output.Add(OutputKeyEvolution, evolution);
        }

        private void MapLevel(ICreature input, IDictionary<string, object> output) {
            var level = new Dictionary<string, object>();

            if (input.Level != null) {
                this.LevelEntityMapper.MapOne(input.Level, level);
            }

            output.Add(OutputKeyLevel, level);
        }

        private void MapNextCreature(ICreature input, IDictionary<string, object> output) {
            var nextCreature = new Dictionary<string, object>();

            if (input.NextCreature != null) {
                this.MapOne(input.NextCreature, nextCreature);
            }

            output.Add(OutputKeyNextCreature, nextCreature);
        }

        private void MapPreviousCreature(ICreature input, IDictionary<string, object> output) {
            var previousCreature = new Dictionary<string, object>();

            if (input.PreviousCreature != null) {
                this.MapOne(input.PreviousCreature, previousCreature);
            }

            output.Add(OutputKeyPreviousCreature, previousCreature);
        }

        private void MapRarity(ICreature input, IDictionary<string, object> output) {
            var rarity = new Dictionary<string, object>();

            if (input.Rarity != null) {
                this.RarityEntityMapper.MapOne(input.Rarity, rarity);
            }

            output.Add(OutputKeyRarity, rarity);
        }

        private void MapSize(ICreature input, IDictionary<string, object> output) {
            var size = new Dictionary<string, object>();

            if (input.Size != null) {
                this.SizeEntityMapper.MapOne(input.Size, size);
            }

            output.Add(OutputKeySize, size);
        }

    }

}