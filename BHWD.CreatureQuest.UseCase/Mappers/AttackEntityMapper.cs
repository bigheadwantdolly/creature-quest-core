﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.UseCase.Mappers {

    public class AttackEntityMapper : BaseEntityMapper<IAttack, Dictionary<string, object>> {

        public const string OutputKeyName = "name";

        public override void MapOne(IAttack input, Dictionary<string, object> output) {
            output.Add(OutputKeyId, input.Id);
            output.Add(OutputKeyName, input.Name);
        }

    }

}