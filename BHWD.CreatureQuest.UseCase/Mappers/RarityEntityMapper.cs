﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.UseCase.Mappers {

    public class RarityEntityMapper : BaseEntityMapper<IRarity, Dictionary<string, object>> {

        public const string OutputKeyName = "name";

        public override void MapOne(IRarity input, Dictionary<string, object> output) {
            output.Add(OutputKeyId, input.Id);
            output.Add(OutputKeyName, input.Name);
        }

    }

}