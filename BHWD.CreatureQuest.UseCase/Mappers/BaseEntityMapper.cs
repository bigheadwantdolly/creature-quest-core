﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;

namespace BHWD.CreatureQuest.UseCase.Mappers {

    public abstract class BaseEntityMapper<TInput, TOutput> : IEntityMapper<TInput, TOutput> where TInput : IEntity where TOutput : new() {

        public const string OutputKeyId = "id";

        public abstract void MapOne(TInput input, TOutput output);

        public void MapMany(IEnumerable<TInput> inputs, List<TOutput> outputs) {
            foreach (var input in inputs) {
                var output = new TOutput();

                this.MapOne(input, output);

                outputs.Add(output);
            }
        }

    }

}