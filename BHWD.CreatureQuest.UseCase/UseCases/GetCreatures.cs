﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.UseCases {

    public class GetCreatures : IGetCreatures {

        private readonly IGetManyCreatureGateway GetManyCreatureGateway;
        private readonly IEntityMapper<ICreature, Dictionary<string, object>> CreatureEntityMapper;

        public GetCreatures(IGetManyCreatureGateway getManyCreatureGateway, IEntityMapper<ICreature, Dictionary<string, object>> creatureEntityMapper) {
            this.GetManyCreatureGateway = getManyCreatureGateway;
            this.CreatureEntityMapper = creatureEntityMapper;
        }

        public void Execute(IGetCreaturesInput input, IGetCreaturesOutput output) {
            var creatures = this.GetManyCreatureGateway.GetAll(input.ColorIds, input.EvolutionIds, input.SizeIds);

            this.CreatureEntityMapper.MapMany(creatures, output.Creatures);
        }

    }

}