﻿using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.UseCases {

    public class GetEvolutions : IGetEvolutions {

        private readonly IGetManyEntityGateway<IEvolution> GetManyEvolutionGateway;
        private readonly IEntityMapper<IEvolution, Dictionary<string, object>> EvolutionEntityMapper;

        public GetEvolutions(IGetManyEntityGateway<IEvolution> getManyEvolutionGateway, IEntityMapper<IEvolution, Dictionary<string, object>> evolutionEntityMapper) {
            this.GetManyEvolutionGateway = getManyEvolutionGateway;
            this.EvolutionEntityMapper = evolutionEntityMapper;
        }

        public void Execute(IGetEvolutionsInput input, IGetEvolutionsOutput output) {
            var evolutions = this.GetManyEvolutionGateway.GetAll();

            evolutions = this.FilterEvolutions(input, evolutions);

            this.EvolutionEntityMapper.MapMany(evolutions, output.Evolutions);
        }

        private List<IEvolution> FilterEvolutions(IGetEvolutionsInput input, IEnumerable<IEvolution> evolutions) {
            var result = evolutions.ToList();
            var matchesFilter = new List<IEvolution>();
            var filtered = false;

            if (input.SizeIds.Count > 0) {
                filtered = true;

                foreach (var evolution in evolutions) {
                    foreach (var filterSize in input.SizeIds) {
                        if (evolution.Sizes.FirstOrDefault(size => filterSize.Equals(size.Id)) != null) {
                            matchesFilter.Add(evolution);
                            break;
                        }
                    }
                }
            }

            if (input.RarityIds.Count > 0) {
                filtered = true;

                foreach (var evolution in evolutions) {
                    foreach (var filterRarity in input.RarityIds) {
                        if (evolution.Rarities.FirstOrDefault(rarity => filterRarity.Equals(rarity.Id)) != null) {
                            matchesFilter.Add(evolution);
                            break;
                        }
                    }
                }
            }

            if (filtered) {
                result = result.Where(evolution => matchesFilter.Contains(evolution)).ToList();
            }

            return result;
        }

    }

}