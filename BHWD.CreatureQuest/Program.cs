﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.Entity.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.Mappers;
using BHWD.CreatureQuest.UseCase.UseCases;

namespace BHWD.CreatureQuest {

    class Program {

        static void Main(string[] args) {
            RegisterEntities();

            var inMemoryEntityGateway = new Bootstrap();
            var webApi = new WebApi.Bootstrap();

            inMemoryEntityGateway.BootstrapModule();

            RegisterUseCases();

            webApi.BootstrapModule();
        }

        private static void RegisterEntities() {
            var entityFactory = EntityFactory.Instance;

            entityFactory.Register<IAttack>(typeof(Attack));
            entityFactory.Register<IColor>(typeof(Color));
            entityFactory.Register<ICreature>(typeof(Creature));
            entityFactory.Register<IEvolution>(typeof(Evolution));
            entityFactory.Register<ILevel>(typeof(Level));
            entityFactory.Register<IRarity>(typeof(Rarity));
            entityFactory.Register<ISize>(typeof(Size));
        }

        private static void RegisterUseCases() {
            var useCaseContainer = UseCaseContainer.Instance;
            var entityGatewayContainer = EntityGatewayContainer.Instance;

            var getManyAttackGateway = entityGatewayContainer.Get<IGetManyEntityGateway<IAttack>>();
            useCaseContainer.Register<IGetAttacks, IGetAttacksInput, IGetAttacksOutput>(new GetAttacks(getManyAttackGateway, new AttackEntityMapper()));

            var getManyColorGateway = entityGatewayContainer.Get<IGetManyEntityGateway<IColor>>();
            useCaseContainer.Register<IGetColors, IGetColorsInput, IGetColorsOutput>(new GetColors(getManyColorGateway, new ColorEntityMapper()));

            var createCreatureGateway = entityGatewayContainer.Get<ICreateEntityGateway<ICreature>>();
            var creatureMapper = new CreatureEntityMapper(
                new AttackEntityMapper(),
                new ColorEntityMapper(),
                new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()),
                new LevelEntityMapper(new LevelRequirementEntityMapper(new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()), new SizeEntityMapper())),
                new RarityEntityMapper(),
                new SizeEntityMapper());
            var getOneAttackGateway = entityGatewayContainer.Get<IGetOneEntityGateway<IAttack>>();
            var getOneColorGateway = entityGatewayContainer.Get<IGetOneEntityGateway<IColor>>();
            var getOneCreatureGateway = entityGatewayContainer.Get<IGetOneEntityGateway<ICreature>>();
            var getOneEvolutionGateway = entityGatewayContainer.Get<IGetOneEntityGateway<IEvolution>>();
            var getOneLevelGateway = entityGatewayContainer.Get<IGetOneEntityGateway<ILevel>>();
            var getOneRarityGateway = entityGatewayContainer.Get<IGetOneEntityGateway<IRarity>>();
            var getOneSizeGateway = entityGatewayContainer.Get<IGetOneEntityGateway<ISize>>();
            var getManyCreatureGateway = entityGatewayContainer.Get<IGetManyCreatureGateway>();
            useCaseContainer.Register<ICreateCreature, ICreateCreatureInput, ICreateCreatureOutput>(
                new CreateCreature(
                    createCreatureGateway,
                    creatureMapper,
                    EntityFactory.Instance,
                    getOneAttackGateway,
                    getOneColorGateway,
                    getOneCreatureGateway,
                    getOneEvolutionGateway,
                    getOneLevelGateway,
                    getOneRarityGateway,
                    getOneSizeGateway,
                    getManyCreatureGateway)
                );

            useCaseContainer.Register<IGetCreatures, IGetCreaturesInput, IGetCreaturesOutput>(new GetCreatures(getManyCreatureGateway, creatureMapper));

            var evolutionMapper = new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper());
            var getManyEvolutionGateway = entityGatewayContainer.Get<IGetManyEntityGateway<IEvolution>>();
            useCaseContainer.Register<IGetEvolutions, IGetEvolutionsInput, IGetEvolutionsOutput>(new GetEvolutions(getManyEvolutionGateway, evolutionMapper));

            var levelMapper = new LevelEntityMapper(new LevelRequirementEntityMapper(new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()), new SizeEntityMapper()));
            var getManyLevelGateway = entityGatewayContainer.Get<IGetManyEntityGateway<ILevel>>();
            useCaseContainer.Register<IGetLevels, IGetLevelsInput, IGetLevelsOutput>(new GetLevels(getManyLevelGateway, levelMapper));

            var getManyRarityGateway = entityGatewayContainer.Get<IGetManyEntityGateway<IRarity>>();
            useCaseContainer.Register<IGetRarities, IGetRaritiesInput, IGetRaritiesOutput>(new GetRarities(getManyRarityGateway, new RarityEntityMapper()));

            var getManySizeGateway = entityGatewayContainer.Get<IGetManyEntityGateway<ISize>>();
            useCaseContainer.Register<IGetSizes, IGetSizesInput, IGetSizesOutput>(new GetSizes(getManySizeGateway, new SizeEntityMapper()));
        }

    }

}