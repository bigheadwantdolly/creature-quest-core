﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.WebApi.Contracts.Mappers {

    public interface IMapper<in TInput, TOutput> {

        TOutput MapOne(TInput input, TOutput output);
        List<TOutput> MapMany(IEnumerable<TInput> inputList, List<TOutput> outputList);

    }

}