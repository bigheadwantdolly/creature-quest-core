﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.DTO.UseCases;
using Microsoft.AspNetCore.Mvc;

namespace BHWD.CreatureQuest.WebApi.Controllers {

    [Route("[controller]")]
    public class CreaturesController : Controller {

        private readonly IApiResponse ApiResponse;
        private readonly ICreateCreature CreateCreatureUseCase;
        private readonly IGetCreatures GetCreaturesUseCase;

        public CreaturesController(IApiResponse apiResponse, ICreateCreature createCreatureUseCase, IGetCreatures getCreaturesUseCase) {
            this.ApiResponse = apiResponse;
            this.CreateCreatureUseCase = createCreatureUseCase;
            this.GetCreaturesUseCase = getCreaturesUseCase;
        }

        [HttpGet]
        public IActionResult Get(
            [FromServices] IGetCreaturesInput getCreaturesInput,
            [FromServices] IGetCreaturesOutput getCreaturesOutput,
            [FromQuery(Name = "color")] List<Guid> colorIds,
            [FromQuery(Name = "evolution")] List<Guid> evolutionIds,
            [FromQuery(Name = "size")] List<Guid> sizeIds) {
            getCreaturesInput.ColorIds = colorIds;
            getCreaturesInput.EvolutionIds = evolutionIds;
            getCreaturesInput.SizeIds = sizeIds;

            this.GetCreaturesUseCase.Execute(getCreaturesInput, getCreaturesOutput);

            return this.Json(this.ApiResponse.GetResponse(getCreaturesOutput));
        }

        [HttpPost]
        public IActionResult Post(ICreateCreatureInput createCreatureInput, [FromServices] ICreateCreatureOutput createCreatureOutput) {
            if (this.ModelState.IsValid) {
                this.CreateCreatureUseCase.Execute(createCreatureInput, createCreatureOutput);
            } else {
                return this.BadRequest(this.ApiResponse.GetResponse(createCreatureOutput, this.ModelState));
            }

            return this.Json(this.ApiResponse.GetResponse(createCreatureOutput));
        }

    }

}