﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BHWD.CreatureQuest.WebApi.Controllers {

    [Route("[controller]")]
    public class AttacksController : Controller {

        private readonly IApiResponse ApiResponse;
        private readonly IGetAttacks GetAttacksUseCase;

        public AttacksController(IApiResponse apiResponse, IGetAttacks getAttacksUseCase) {
            this.ApiResponse = apiResponse;
            this.GetAttacksUseCase = getAttacksUseCase;
        }

        [HttpGet]
        public IActionResult Get([FromServices] IGetAttacksInput getAttacksInput, [FromServices] IGetAttacksOutput getAttacksOutput) {
            this.GetAttacksUseCase.Execute(getAttacksInput, getAttacksOutput);

            return this.Json(this.ApiResponse.GetResponse(getAttacksOutput));
        }

    }

}