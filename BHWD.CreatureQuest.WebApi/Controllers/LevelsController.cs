﻿using System;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BHWD.CreatureQuest.WebApi.Controllers {

    [Route("[controller]")]
    public class LevelsController : Controller {

        private readonly IApiResponse ApiResponse;
        private readonly IGetLevels GetLevelsUseCase;

        public LevelsController(IApiResponse apiResponse, IGetLevels getLevelsUseCase) {
            this.ApiResponse = apiResponse;
            this.GetLevelsUseCase = getLevelsUseCase;
        }

        [HttpGet]
        public IActionResult Get(
            [FromServices] IGetLevelsInput getLevelsInput,
            [FromServices] IGetLevelsOutput getLevelsOutput,
            [FromQuery(Name = "evolution")] Guid evolutionId,
            [FromQuery(Name = "size")] Guid sizeId) {
            getLevelsInput.EvolutionId = evolutionId;
            getLevelsInput.SizeId = sizeId;

            this.GetLevelsUseCase.Execute(getLevelsInput, getLevelsOutput);

            return this.Json(this.ApiResponse.GetResponse(getLevelsOutput));
        }

    }

}