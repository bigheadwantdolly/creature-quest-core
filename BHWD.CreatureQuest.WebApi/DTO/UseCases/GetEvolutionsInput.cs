﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetEvolutionsInput : IGetEvolutionsInput {

        public List<Guid> RarityIds { get; set; }
        public List<Guid> SizeIds { get; set; }

        public GetEvolutionsInput() {
            this.RarityIds = new List<Guid>();
            this.SizeIds = new List<Guid>();
        }

    }

}