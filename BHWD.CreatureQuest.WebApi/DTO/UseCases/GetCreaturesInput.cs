﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetCreaturesInput : IGetCreaturesInput {

        public List<Guid> ColorIds { get; set; }
        public List<Guid> EvolutionIds { get; set; }
        public List<Guid> SizeIds { get; set; }

        public GetCreaturesInput() {
            this.ColorIds = new List<Guid>();
            this.EvolutionIds = new List<Guid>();
            this.SizeIds = new List<Guid>();
        }

    }

}