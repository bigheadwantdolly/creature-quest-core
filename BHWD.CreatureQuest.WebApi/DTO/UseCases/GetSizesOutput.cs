﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetSizesOutput : IGetSizesOutput {

        public List<Dictionary<string, object>> Sizes { get; set; } = new List<Dictionary<string, object>>();

    }

}