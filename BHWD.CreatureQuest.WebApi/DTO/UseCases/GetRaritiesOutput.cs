﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetRaritiesOutput : IGetRaritiesOutput {

        public List<Dictionary<string, object>> Rarities { get; set; } = new List<Dictionary<string, object>>();

    }

}