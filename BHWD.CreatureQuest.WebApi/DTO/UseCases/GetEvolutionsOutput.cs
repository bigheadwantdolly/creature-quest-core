﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetEvolutionsOutput : IGetEvolutionsOutput {

        public List<Dictionary<string, object>> Evolutions { get; set; } = new List<Dictionary<string, object>>();

    }

}