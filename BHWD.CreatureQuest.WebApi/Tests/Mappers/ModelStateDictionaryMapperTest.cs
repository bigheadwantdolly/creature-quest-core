﻿using System.Collections.Generic;
using BHWD.CreatureQuest.WebApi.Mappers;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NUnit.Framework;

namespace BHWD.CreatureQuest.WebApi.Tests.Mappers {

    [TestFixture]
    internal class ModelStateDictionaryMapperTest {

        private ModelStateDictionaryMapper ModelStateDictionaryMapper;
        private ModelStateDictionary ModelStateDictionary;

        [SetUp]
        public void SetUp() {
            this.ModelStateDictionary = new ModelStateDictionary();
            this.ModelStateDictionaryMapper = new ModelStateDictionaryMapper();

            this.ModelStateDictionary.AddModelError("thing", "Thing");
            this.ModelStateDictionary.AddModelError("thing2", "Thing 2");
        }

        [Test]
        public void MapsOne() {
            var errorList = new List<string>();

            this.ModelStateDictionaryMapper.MapOne(this.ModelStateDictionary, errorList);
            var expected = new List<string> {
                "Thing",
                "Thing 2"
            };

            Assert.AreEqual(expected, errorList);
        }

        [Test]
        public void MapsOneAndReturnsValue() {
            var errorList = new List<string>();

            var actual = this.ModelStateDictionaryMapper.MapOne(this.ModelStateDictionary, errorList);
            var expected = new List<string> {
                "Thing",
                "Thing 2"
            };

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void MapsOneThrowsNoExceptionIfInputIsNull() {
            var errorList = new List<string>();

            this.ModelStateDictionaryMapper.MapOne(null, errorList);

            Assert.IsEmpty(errorList);
        }

        [Test]
        public void MapsOneThrowsNoExceptionIfOutputIsNull() {
            this.ModelStateDictionaryMapper.MapOne(this.ModelStateDictionary, null);

            Assert.IsTrue(true);
        }

        [Test]
        public void MapsMany() {
            var outputList = new List<List<string>>();
            var inputList = new List<ModelStateDictionary> {
                this.ModelStateDictionary,
                this.ModelStateDictionary,
                this.ModelStateDictionary
            };

            this.ModelStateDictionaryMapper.MapMany(inputList, outputList);
            const int expected = 3;

            Assert.AreEqual(expected, outputList.Count);
        }

        [Test]
        public void MapsManyAndReturnsValue() {
            var outputList = new List<List<string>>();
            var inputList = new List<ModelStateDictionary> {
                this.ModelStateDictionary,
                this.ModelStateDictionary,
                this.ModelStateDictionary
            };

            var actual = this.ModelStateDictionaryMapper.MapMany(inputList, outputList);
            const int expected = 3;

            Assert.AreEqual(expected, actual.Count);
        }

        [Test]
        public void MapsManyThrowsNoExceptionIfInputIsNull() {
            var outputList = new List<List<string>>();

            this.ModelStateDictionaryMapper.MapMany(null, outputList);

            Assert.IsEmpty(outputList);
        }

        [Test]
        public void MapsManyThrowsNoExceptionIfOutputIsNull() {
            var inputList = new List<ModelStateDictionary> {
                this.ModelStateDictionary,
                this.ModelStateDictionary,
                this.ModelStateDictionary
            };
            this.ModelStateDictionaryMapper.MapMany(inputList, null);

            Assert.IsTrue(true);
        }

    }

}