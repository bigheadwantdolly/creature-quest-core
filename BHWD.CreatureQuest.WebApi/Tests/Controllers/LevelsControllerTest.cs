﻿using System;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Controllers;
using Moq;
using NUnit.Framework;

namespace BHWD.CreatureQuest.WebApi.Tests.Controllers {

    [TestFixture]
    internal class LevelsControllerTest {

        private Mock<IApiResponse> ApiResponse;
        private LevelsController LevelsController { get; set; }
        private Mock<IGetLevels> UseCase { get; set; }

        [SetUp]
        public void SetUp() {
            this.ApiResponse = new Mock<IApiResponse>();
            this.UseCase = new Mock<IGetLevels>();
            this.LevelsController = new LevelsController(this.ApiResponse.Object, this.UseCase.Object);
        }

        [Test]
        public void GetExecutesUseCase() {
            var useCaseInput = new Mock<IGetLevelsInput>();
            var useCaseOutput = new Mock<IGetLevelsOutput>();

            this.LevelsController.Get(useCaseInput.Object, useCaseOutput.Object, Guid.Empty, Guid.Empty);

            this.UseCase.Verify(x => x.Execute(It.IsAny<IGetLevelsInput>(), It.IsAny<IGetLevelsOutput>()), Times.Once);
        }

        [Test]
        public void GetInjectsEvolutionIdIntoUseCaseInput() {
            var useCaseInput = new Mock<IGetLevelsInput>();
            var useCaseOutput = new Mock<IGetLevelsOutput>();
            var evolutionId = Guid.NewGuid();

            this.LevelsController.Get(useCaseInput.Object, useCaseOutput.Object, evolutionId, Guid.Empty);

            useCaseInput.VerifySet(x => x.EvolutionId = evolutionId);
        }

        [Test]
        public void GetInjectsSizeIdIntoUseCaseInput() {
            var useCaseInput = new Mock<IGetLevelsInput>();
            var useCaseOutput = new Mock<IGetLevelsOutput>();
            var sizeId = Guid.NewGuid();

            this.LevelsController.Get(useCaseInput.Object, useCaseOutput.Object, Guid.Empty, sizeId);

            useCaseInput.VerifySet(x => x.SizeId = sizeId);
        }

        [Test]
        public void GetReturnsOutput() {
            var useCaseInput = new Mock<IGetLevelsInput>();
            var useCaseOutput = new Mock<IGetLevelsOutput>();

            var output = this.LevelsController.Get(useCaseInput.Object, useCaseOutput.Object, Guid.Empty, Guid.Empty);

            Assert.That(output, Is.Not.Null);
        }

    }

}