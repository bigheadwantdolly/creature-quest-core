﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace BHWD.CreatureQuest.WebApi.Tests.Controllers {

    [TestFixture]
    internal class CreaturesControllerTest {

        private Mock<IApiResponse> ApiResponse;
        private Mock<ICreateCreature> CreateCreatureUseCase { get; set; }
        private CreaturesController CreaturesController { get; set; }
        private Mock<IGetCreatures> GetCreaturesUseCase { get; set; }

        [SetUp]
        public void SetUp() {
            this.ApiResponse = new Mock<IApiResponse>();
            this.CreateCreatureUseCase = new Mock<ICreateCreature>();
            this.GetCreaturesUseCase = new Mock<IGetCreatures>();
            this.CreaturesController = new CreaturesController(this.ApiResponse.Object, this.CreateCreatureUseCase.Object, this.GetCreaturesUseCase.Object);
        }

        [Test]
        public void GetExecutesUseCase() {
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();

            this.CreaturesController.Get(useCaseInput.Object, useCaseOutput.Object, new List<Guid>(), new List<Guid>(), new List<Guid>());

            this.GetCreaturesUseCase.Verify(x => x.Execute(It.IsAny<IGetCreaturesInput>(), It.IsAny<IGetCreaturesOutput>()), Times.Once);
        }

        [Test]
        public void GetInjectsColorIdsIntoUseCaseInput() {
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();
            var colorIds = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };

            this.CreaturesController.Get(useCaseInput.Object, useCaseOutput.Object, colorIds, null, null);

            useCaseInput.VerifySet(x => x.ColorIds = colorIds);
        }

        [Test]
        public void GetInjectsEvolutionIdsIntoUseCaseInput() {
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();
            var evolutionIds = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };

            this.CreaturesController.Get(useCaseInput.Object, useCaseOutput.Object, null, evolutionIds, null);

            useCaseInput.VerifySet(x => x.EvolutionIds = evolutionIds);
        }

        [Test]
        public void GetInjectsSizeIdsIntoUseCaseInput() {
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();
            var sizeIds = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };

            this.CreaturesController.Get(useCaseInput.Object, useCaseOutput.Object, null, null, sizeIds);

            useCaseInput.VerifySet(x => x.SizeIds = sizeIds);
        }

        [Test]
        public void GetReturnsOutput() {
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();
            var output = this.CreaturesController.Get(useCaseInput.Object, useCaseOutput.Object, new List<Guid>(), new List<Guid>(), new List<Guid>());

            Assert.That(output, Is.Not.Null);
        }

        [Test]
        public void GetReturnsJsonOutput() {
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();
            var output = this.CreaturesController.Get(useCaseInput.Object, useCaseOutput.Object, new List<Guid>(), new List<Guid>(), new List<Guid>());

            Assert.IsInstanceOf<JsonResult>(output);
        }

        [Test]
        public void PostReturnsJsonOutput() {
            var useCaseInput = new Mock<ICreateCreatureInput>();
            var useCaseOutput = new Mock<ICreateCreatureOutput>();
            var output = this.CreaturesController.Post(useCaseInput.Object, useCaseOutput.Object);

            Assert.IsInstanceOf<JsonResult>(output);
        }

        [Test]
        public void PostReturnsBadRequestOutput() {
            var useCaseInput = new Mock<ICreateCreatureInput>();
            var useCaseOutput = new Mock<ICreateCreatureOutput>();

            this.CreaturesController.ModelState.AddModelError("test", "test");
            var output = this.CreaturesController.Post(useCaseInput.Object, useCaseOutput.Object);

            Assert.IsInstanceOf<BadRequestObjectResult>(output);
        }

        [Test]
        public void PostExecutesUseCase() {
            var useCaseInput = new Mock<ICreateCreatureInput>();
            var useCaseOutput = new Mock<ICreateCreatureOutput>();

            this.CreaturesController.Post(useCaseInput.Object, useCaseOutput.Object);

            this.CreateCreatureUseCase.Verify(x => x.Execute(It.IsAny<ICreateCreatureInput>(), It.IsAny<ICreateCreatureOutput>()), Times.Once);
        }

        [Test]
        public void PostDoesNotExecuteUseCase() {
            var useCaseInput = new Mock<ICreateCreatureInput>();
            var useCaseOutput = new Mock<ICreateCreatureOutput>();

            this.CreaturesController.ModelState.AddModelError("test", "test");
            this.CreaturesController.Post(useCaseInput.Object, useCaseOutput.Object);

            this.CreateCreatureUseCase.Verify(x => x.Execute(It.IsAny<ICreateCreatureInput>(), It.IsAny<ICreateCreatureOutput>()), Times.Never);
        }

    }

}