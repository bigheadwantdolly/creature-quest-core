﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Controllers;
using Moq;
using NUnit.Framework;

namespace BHWD.CreatureQuest.WebApi.Tests.Controllers {

    [TestFixture]
    internal class EvolutionsControllerTest {

        private Mock<IApiResponse> ApiResponse;
        private EvolutionsController EvolutionsController { get; set; }
        private Mock<IGetEvolutions> UseCase { get; set; }

        [SetUp]
        public void SetUp() {
            this.ApiResponse = new Mock<IApiResponse>();
            this.UseCase = new Mock<IGetEvolutions>();
            this.EvolutionsController = new EvolutionsController(this.ApiResponse.Object, this.UseCase.Object);
        }

        [Test]
        public void GetExecutesUseCase() {
            var useCaseInput = new Mock<IGetEvolutionsInput>();
            var useCaseOutput = new Mock<IGetEvolutionsOutput>();

            this.EvolutionsController.Get(useCaseInput.Object, useCaseOutput.Object, new List<Guid>(), new List<Guid>());

            this.UseCase.Verify(x => x.Execute(It.IsAny<IGetEvolutionsInput>(), It.IsAny<IGetEvolutionsOutput>()), Times.Once);
        }

        [Test]
        public void GetInjectsSizeIdsIntoUseCaseInput() {
            var useCaseInput = new Mock<IGetEvolutionsInput>();
            var useCaseOutput = new Mock<IGetEvolutionsOutput>();
            var sizeIds = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };

            this.EvolutionsController.Get(useCaseInput.Object, useCaseOutput.Object, new List<Guid>(), sizeIds);

            useCaseInput.VerifySet(x => x.SizeIds = sizeIds);
        }

        [Test]
        public void GetInjectsRarityIdsIntoUseCaseInput() {
            var useCaseInput = new Mock<IGetEvolutionsInput>();
            var useCaseOutput = new Mock<IGetEvolutionsOutput>();
            var rarityIds = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };

            this.EvolutionsController.Get(useCaseInput.Object, useCaseOutput.Object, rarityIds, new List<Guid>());

            useCaseInput.VerifySet(x => x.RarityIds = rarityIds);
        }

        [Test]
        public void GetReturnsOutput() {
            var useCaseInput = new Mock<IGetEvolutionsInput>();
            var useCaseOutput = new Mock<IGetEvolutionsOutput>();
            var output = this.EvolutionsController.Get(useCaseInput.Object, useCaseOutput.Object, new List<Guid>(), new List<Guid>());

            Assert.That(output, Is.Not.Null);
        }

    }

}