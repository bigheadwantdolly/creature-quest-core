﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Controllers;
using Moq;
using NUnit.Framework;

namespace BHWD.CreatureQuest.WebApi.Tests.Controllers {

    [TestFixture]
    internal class ColorsControllerTest {

        private Mock<IApiResponse> ApiResponse;
        private ColorsController ColorsController { get; set; }
        private Mock<IGetColors> UseCase { get; set; }

        [SetUp]
        public void SetUp() {
            this.ApiResponse = new Mock<IApiResponse>();
            this.UseCase = new Mock<IGetColors>();
            this.ColorsController = new ColorsController(this.ApiResponse.Object, this.UseCase.Object);
        }

        [Test]
        public void GetExecutesUseCase() {
            var useCaseInput = new Mock<IGetColorsInput>();
            var useCaseOutput = new Mock<IGetColorsOutput>();

            this.ColorsController.Get(useCaseInput.Object, useCaseOutput.Object);

            this.UseCase.Verify(x => x.Execute(It.IsAny<IGetColorsInput>(), It.IsAny<IGetColorsOutput>()), Times.Once);
        }

        [Test]
        public void GetReturnsOutput() {
            var useCaseInput = new Mock<IGetColorsInput>();
            var useCaseOutput = new Mock<IGetColorsOutput>();
            var output = this.ColorsController.Get(useCaseInput.Object, useCaseOutput.Object);

            Assert.That(output, Is.Not.Null);
        }

    }

}