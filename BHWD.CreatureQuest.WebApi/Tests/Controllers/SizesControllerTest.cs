﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Controllers;
using Moq;
using NUnit.Framework;

namespace BHWD.CreatureQuest.WebApi.Tests.Controllers {

    [TestFixture]
    internal class SizesControllerTest {

        private Mock<IApiResponse> ApiResponse;
        private SizesController SizesController { get; set; }
        private Mock<IGetSizes> UseCase { get; set; }

        [SetUp]
        public void SetUp() {
            this.ApiResponse = new Mock<IApiResponse>();
            this.UseCase = new Mock<IGetSizes>();
            this.SizesController = new SizesController(this.ApiResponse.Object, this.UseCase.Object);
        }

        [Test]
        public void GetExecutesUseCase() {
            var useCaseInput = new Mock<IGetSizesInput>();
            var useCaseOutput = new Mock<IGetSizesOutput>();

            this.SizesController.Get(useCaseInput.Object, useCaseOutput.Object);

            this.UseCase.Verify(x => x.Execute(It.IsAny<IGetSizesInput>(), It.IsAny<IGetSizesOutput>()), Times.Once);
        }

        [Test]
        public void GetReturnsOutput() {
            var useCaseInput = new Mock<IGetSizesInput>();
            var useCaseOutput = new Mock<IGetSizesOutput>();
            var output = this.SizesController.Get(useCaseInput.Object, useCaseOutput.Object);

            Assert.That(output, Is.Not.Null);
        }

    }

}