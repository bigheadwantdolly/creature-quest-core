﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaders;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSetHydrators;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityDataSetHydrators {

    public class EntityDataSetHydrator<TEntity> : IEntityDataSetHydrator<TEntity> where TEntity : IEntity {

        public void Execute(IEntityDataLoader<TEntity> entityDataLoader, IEntityDataSet<TEntity> entityDataSet) {
            entityDataSet.Entities.AddRange(entityDataLoader.Execute());
        }

    }

}