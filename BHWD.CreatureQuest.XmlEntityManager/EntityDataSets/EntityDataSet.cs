﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityDataSets {

    public class EntityDataSet<TEntity> : IEntityDataSet<TEntity> where TEntity : IEntity {

        public List<TEntity> Entities { get; set; }

        public EntityDataSet() {
            this.Entities = new List<TEntity>();
        }

    }

}