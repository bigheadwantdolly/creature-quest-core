﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityDataSets {

    public class EntityDataSetContainer : IEntityDataSetContainer {

        public static IEntityDataSetContainer Instance { get; set; } = new EntityDataSetContainer();

        private Dictionary<Type, object> EntityDataSets { get; } = new Dictionary<Type, object>();

        public bool Register<TEntity>(IEntityDataSet<TEntity> entityDataSet) where TEntity : IEntity {
            var type = typeof(TEntity);
            var registered = true;

            try {
                this.EntityDataSets.Add(type, entityDataSet);
            } catch (Exception exception) {
                Console.WriteLine(exception);
                registered = false;
            }

            return registered;
        }

        public IEntityDataSet<TEntity> Get<TEntity>() where TEntity : IEntity {
            var type = typeof(TEntity);

            this.EntityDataSets.TryGetValue(type, out var output);

            return (IEntityDataSet<TEntity>) output;
        }

    }

}