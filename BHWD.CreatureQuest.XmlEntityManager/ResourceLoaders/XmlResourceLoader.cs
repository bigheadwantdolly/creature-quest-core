﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.XPath;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.ResourceLoaders;

namespace BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders {

    public class XmlResourceLoader : IResourceLoader<XPathDocument> {

        public XPathDocument Execute(string resourceName) {
            var stream = this.GetResourceStream(resourceName);
            XPathDocument xPathDocument = null;

            if (stream != null) {
                xPathDocument = new XPathDocument(stream);
            }

            return xPathDocument;
        }

        private Stream GetResourceStream(string resourceName) {
            var assembly = Assembly.GetExecutingAssembly();
            var manifestResourceNames = assembly.GetManifestResourceNames();
            var resource = manifestResourceNames.FirstOrDefault(x => x.EndsWith(resourceName));
            Stream stream = null;

            if (!string.IsNullOrEmpty(resource)) {
                stream = assembly.GetManifestResourceStream(resource);
            }

            return stream;
        }

    }

}