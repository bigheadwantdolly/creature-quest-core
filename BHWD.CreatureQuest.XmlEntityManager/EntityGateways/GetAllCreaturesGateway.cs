﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityGateways {

    public class GetAllCreaturesGateway : IGetManyEntityGateway<ICreature> {

        private readonly IEntityDataSet<ICreature> EntityDataSet;

        public GetAllCreaturesGateway(IEntityDataSet<ICreature> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<ICreature> GetAll() {
            return this.EntityDataSet.Entities;
        }

    }

}