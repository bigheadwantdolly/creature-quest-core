﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaderIterators;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators {

    public class EntityDataLoaderIterator : IEntityDataLoaderIterator<XPathDocument, XPathNavigator> {

        public List<XPathNavigator> GetList(XPathDocument input, string rootNode) {
            var xPathNavigator = input.CreateNavigator();
            var xPathNavigators = xPathNavigator.Select("//" + rootNode + "/*").Cast<XPathNavigator>().ToList();

            return xPathNavigators;
        }

    }

}