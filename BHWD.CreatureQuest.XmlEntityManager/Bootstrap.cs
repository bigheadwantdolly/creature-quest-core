﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.EntityGateways;
using BHWD.CreatureQuest.Plugin;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaders;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataSetHydrators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataSets;
using BHWD.CreatureQuest.XmlEntityManager.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;

namespace BHWD.CreatureQuest.XmlEntityManager {

    public class Bootstrap : IPlugin {

        public string Description => "XML Data Store";

        public void BootstrapModule() {
            Console.WriteLine(this.Description + " Starting Up!");

            this.LoadData();
            this.SetupEntityGateways();
        }

        private void LoadData() {
            var attackDataSet = this.LoadResourceData(
                new List<string> {
                    "attacks.xml"
                },
                "attacks",
                new AttackDataMapper());
            var colorDataSet = this.LoadResourceData(
                new List<string> {
                    "colors.xml"
                },
                "colors",
                new ColorDataMapper());
            var rarityDataSet = this.LoadResourceData(
                new List<string> {
                    "rarities.xml"
                },
                "rarities",
                new RarityDataMapper());
            var sizeDataSet = this.LoadResourceData(
                new List<string> {
                    "sizes.xml"
                },
                "sizes",
                new SizeDataMapper());
            var creatureDataSet = this.LoadResourceData(
                new List<string> {
                    "small-black-creatures.xml",
                    "small-blue-creatures.xml",
                    "small-green-creatures.xml",
                    "small-red-creatures.xml",
                    "small-white-creatures.xml",
                    "small-yellow-creatures.xml"
                },
                "creatures",
                new CreatureDataMapper(colorDataSet, rarityDataSet, sizeDataSet));

            EntityDataSetContainer.Instance.Register(attackDataSet);
            EntityDataSetContainer.Instance.Register(colorDataSet);
            EntityDataSetContainer.Instance.Register(creatureDataSet);
            EntityDataSetContainer.Instance.Register(rarityDataSet);
            EntityDataSetContainer.Instance.Register(sizeDataSet);
        }

        private EntityDataSet<TEntity> LoadResourceData<TEntity>(List<string> resources, string rootNode, IEntityDataMapper<XPathNavigator, TEntity> entityDataMapper)
            where TEntity : IEntity {
            var resourceLoader = new XmlResourceLoader();
            var entityDataLoaderIterator = new EntityDataLoaderIterator();
            var entityDataSet = new EntityDataSet<TEntity>();

            foreach (var resource in resources) {
                var entityDataLoader = new EntityDataLoader<TEntity, XPathDocument, XPathNavigator>(resourceLoader, entityDataLoaderIterator, entityDataMapper, resource, rootNode);
                var entityDataSetHydrator = new EntityDataSetHydrator<TEntity>();

                entityDataSetHydrator.Execute(entityDataLoader, entityDataSet);
            }

            return entityDataSet;
        }

        private void SetupEntityGateways() {
            var attackDataSet = EntityDataSetContainer.Instance.Get<IAttack>();
            var getAllAttacksGateway = new GetAllAttacksGateway(attackDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<IAttack>>(getAllAttacksGateway);

            var colorDataSet = EntityDataSetContainer.Instance.Get<IColor>();
            var getAllColorsGateway = new GetAllColorsGateway(colorDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<IColor>>(getAllColorsGateway);

            var creatureDataSet = EntityDataSetContainer.Instance.Get<ICreature>();
            var getAllCreaturesGateway = new GetAllCreaturesGateway(creatureDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<ICreature>>(getAllCreaturesGateway);

            var rarityDataSet = EntityDataSetContainer.Instance.Get<IRarity>();
            var getAllRaritiesGateway = new GetAllRaritiesGateway(rarityDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<IRarity>>(getAllRaritiesGateway);

            var sizeDataSet = EntityDataSetContainer.Instance.Get<ISize>();
            var getAllSizesGateway = new GetAllSizesGateway(sizeDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<ISize>>(getAllSizesGateway);
        }

    }

}