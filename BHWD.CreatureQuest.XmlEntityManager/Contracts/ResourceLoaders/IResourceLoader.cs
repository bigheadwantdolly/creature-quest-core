﻿namespace BHWD.CreatureQuest.XmlEntityManager.Contracts.ResourceLoaders {

    public interface IResourceLoader<TOutput> {

        TOutput Execute(string resourceName);

    }

}