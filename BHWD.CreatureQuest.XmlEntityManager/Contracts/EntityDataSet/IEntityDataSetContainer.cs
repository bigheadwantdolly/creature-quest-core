﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet {

    public interface IEntityDataSetContainer {

        bool Register<TEntity>(IEntityDataSet<TEntity> entityDataSet) where TEntity : IEntity;

        IEntityDataSet<TEntity> Get<TEntity>() where TEntity : IEntity;

    }

}