﻿using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers {

    public class RarityDataMapper : BaseEntityDataMapper<XPathNavigator, IRarity> {

        public const string SourceKeyName = "name";

        public override void MapOne(XPathNavigator source, IRarity destination) {
            var name = source.GetAttribute(SourceKeyName, source.NamespaceURI);

            destination.Name = name;
        }

    }

}