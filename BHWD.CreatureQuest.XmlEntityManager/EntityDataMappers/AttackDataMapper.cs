﻿using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers {

    public class AttackDataMapper : BaseEntityDataMapper<XPathNavigator, IAttack> {

        public const string SourceKeyName = "name";

        public override void MapOne(XPathNavigator source, IAttack destination) {
            var name = source.GetAttribute(SourceKeyName, source.NamespaceURI);

            destination.Name = name;
        }

    }

}