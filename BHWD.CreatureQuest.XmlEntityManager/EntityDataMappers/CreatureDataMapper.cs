﻿using System.Linq;
using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers {

    public class CreatureDataMapper : BaseEntityDataMapper<XPathNavigator, ICreature> {

        private readonly IEntityDataSet<IColor> ColorDataSet;
        private readonly IEntityDataSet<IRarity> RarityDataSet;
        private readonly IEntityDataSet<ISize> SizeDataSet;

        public const string SourceKeyColor = "color";
        public const string SourceKeyName = "name";
        public const string SourceKeyRarity = "rarity";
        public const string SourceKeySize = "size";

        public CreatureDataMapper(IEntityDataSet<IColor> colorDataSet, IEntityDataSet<IRarity> rarityDataSet, IEntityDataSet<ISize> sizeDataSet) {
            this.ColorDataSet = colorDataSet;
            this.RarityDataSet = rarityDataSet;
            this.SizeDataSet = sizeDataSet;
        }

        public override void MapOne(XPathNavigator source, ICreature destination) {
            var color = source.GetAttribute(SourceKeyColor, source.NamespaceURI);
            var name = source.GetAttribute(SourceKeyName, source.NamespaceURI);
            var rarity = source.GetAttribute(SourceKeyRarity, source.NamespaceURI);
            var size = source.GetAttribute(SourceKeySize, source.NamespaceURI);

            destination.Color = this.ColorDataSet.Entities.FirstOrDefault(x => x.Name.Equals(color));
            destination.Name = name;
            destination.Rarity = this.RarityDataSet.Entities.FirstOrDefault(x => x.Name.Equals(rarity));
            destination.Size = this.SizeDataSet.Entities.FirstOrDefault(x => x.Name.Equals(size));
        }

    }

}