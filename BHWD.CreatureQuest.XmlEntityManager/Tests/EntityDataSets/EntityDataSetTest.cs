﻿using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataSets;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataSets {

    [TestFixture]
    internal class EntityDataSetTest {

        private IEntityDataSet<DummyEntity> EntityDataSet { get; set; }

        [SetUp]
        public void SetUp() {
            this.EntityDataSet = new EntityDataSet<DummyEntity>();
        }

        [Test]
        public void AddToEntities() {
            const int expected = 1;

            this.EntityDataSet.Entities.Add(new DummyEntity());

            var actual = this.EntityDataSet.Entities.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void CanRemoveFromEntities() {
            const int expected = 0;
            var entity = new DummyEntity();

            this.EntityDataSet.Entities.Add(entity);
            this.EntityDataSet.Entities.Remove(entity);

            var actual = this.EntityDataSet.Entities.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}