﻿using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataSets;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataSets {

    [TestFixture]
    internal class EntityDataSetContainerTest {

        private IEntityDataSetContainer EntityDataSetContainer { get; set; }

        [SetUp]
        public void SetUp() {
            this.EntityDataSetContainer = new EntityDataSetContainer();
            XmlEntityManager.EntityDataSets.EntityDataSetContainer.Instance = this.EntityDataSetContainer;
        }

        [Test]
        public void Instances() {
            var actual = XmlEntityManager.EntityDataSets.EntityDataSetContainer.Instance;
            var expected = this.EntityDataSetContainer;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Registers() {
            var entityDataSet = new StubEntityDataSet<IDummyEntity>();
            var actual = this.EntityDataSetContainer.Register(entityDataSet);
            const bool expected = true;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Gets() {
            var entityDataSet = new StubEntityDataSet<IDummyEntity>();
            this.EntityDataSetContainer.Register(entityDataSet);
            var actual = this.EntityDataSetContainer.Get<IDummyEntity>();

            Assert.IsNotNull(actual);
        }

        [Test]
        public void TypesAreRetained() {
            var entityDataSet = new StubEntityDataSet<IDummyEntity>();
            this.EntityDataSetContainer.Register(entityDataSet);
            var actual = this.EntityDataSetContainer.Get<IDummyEntity>().GetType();
            var expected = entityDataSet.GetType();

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}