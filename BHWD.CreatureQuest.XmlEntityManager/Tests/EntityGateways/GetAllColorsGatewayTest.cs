﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityGateways {

    [TestFixture]
    internal class GetAllColorsGatewayTest {

        private List<IColor> Colors { get; set; }
        private IGetManyEntityGateway<IColor> GetAllColorsGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Colors = new List<IColor> {
                new Color(),
                new Color(),
                new Color()
            };
            var entityDataSet = new StubEntityDataSet<IColor>(this.Colors);

            this.GetAllColorsGateway = new GetAllColorsGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetAllColorsGateway.GetAll().Count;
            var expected = this.Colors.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}