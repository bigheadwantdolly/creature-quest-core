﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityGateways {

    [TestFixture]
    internal class GetAllAttacksGatewayTest {

        private List<IAttack> Attacks { get; set; }
        private IGetManyEntityGateway<IAttack> GetAllAttacksGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Attacks = new List<IAttack> {
                new Attack(),
                new Attack(),
                new Attack()
            };
            var entityDataSet = new StubEntityDataSet<IAttack>(this.Attacks);

            this.GetAllAttacksGateway = new GetAllAttacksGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetAllAttacksGateway.GetAll().Count;
            var expected = this.Attacks.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}