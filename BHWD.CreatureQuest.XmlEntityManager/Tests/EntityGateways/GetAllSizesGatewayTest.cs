﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityGateways {

    [TestFixture]
    internal class GetAllSizesGatewayTest {

        private List<ISize> Sizes { get; set; }
        private IGetManyEntityGateway<ISize> GetAllSizesGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Sizes = new List<ISize> {
                new Size(),
                new Size(),
                new Size()
            };
            var entityDataSet = new StubEntityDataSet<ISize>(this.Sizes);

            this.GetAllSizesGateway = new GetAllSizesGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetAllSizesGateway.GetAll().Count;
            var expected = this.Sizes.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}