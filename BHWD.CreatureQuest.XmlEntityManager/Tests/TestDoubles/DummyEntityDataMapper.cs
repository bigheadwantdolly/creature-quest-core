﻿using System.Collections.Generic;
using System.Xml.XPath;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles {

    internal class DummyEntityDataMapper : IEntityDataMapper<XPathNavigator, IDummyEntity> {

        public void MapOne(XPathNavigator source, IDummyEntity destination) {
            // stub
        }

        public void MapMany(List<XPathNavigator> sources, List<IDummyEntity> destinations) {
            // stub
        }

    }

}