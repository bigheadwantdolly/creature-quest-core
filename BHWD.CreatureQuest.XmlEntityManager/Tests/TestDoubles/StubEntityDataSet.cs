﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles {

    internal class StubEntityDataSet<TEntity> : IEntityDataSet<TEntity> where TEntity : IEntity {

        public List<TEntity> Entities { get; set; }

        public StubEntityDataSet() {
            this.Entities = new List<TEntity>();
        }

        public StubEntityDataSet(List<TEntity> entities) {
            this.Entities = entities;
        }

    }

}