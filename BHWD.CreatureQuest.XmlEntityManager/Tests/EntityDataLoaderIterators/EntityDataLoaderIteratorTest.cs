﻿using System.Xml.XPath;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataLoaderIterators {

    [TestFixture]
    internal class EntityDataLoaderIteratorTest {

        private IEntityDataLoaderIterator<XPathDocument, XPathNavigator> EntityDataLoaderIterator { get; set; }
        private string RootNode { get; set; }
        private XPathDocument XPathDocument { get; set; }

        [SetUp]
        public void SetUp() {
            const string resourceName = "test-creatures.xml";
            var xmlResourceLoader = new XmlResourceLoader();
            this.XPathDocument = xmlResourceLoader.Execute(resourceName);

            this.RootNode = "creatures";
            this.EntityDataLoaderIterator = new EntityDataLoaderIterator();
        }

        [Test]
        public void GetList() {
            var actual = this.EntityDataLoaderIterator.GetList(this.XPathDocument, this.RootNode).Count;
            const int expected = 6;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}