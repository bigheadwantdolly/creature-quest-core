﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataLoaderMappers {

    [TestFixture]
    internal class RarityDataLoaderMapperTest {

        private IEntityDataMapper<XPathNavigator, IRarity> EntityDataMapper { get; set; }
        private List<XPathNavigator> XPathNavigators { get; set; }

        [SetUp]
        public void SetUp() {
            const string resourceName = "rarities.xml";
            const string rootNode = "rarities";

            var xmlResourceLoader = new XmlResourceLoader();
            var xPathDocument = xmlResourceLoader.Execute(resourceName);
            var entityDataLoaderIterator = new EntityDataLoaderIterator();
            var entityFactory = new EntityFactory();

            EntityFactory.Instance = entityFactory;
            entityFactory.Register<IRarity>(typeof(Rarity));

            this.XPathNavigators = entityDataLoaderIterator.GetList(xPathDocument, rootNode);
            this.EntityDataMapper = new RarityDataMapper();
        }

        [Test]
        public void MapName() {
            var rarity = new Rarity();
            var xPathNavigator = this.XPathNavigators.FirstOrDefault();
            var expected = xPathNavigator?.GetAttribute(RarityDataMapper.SourceKeyName, xPathNavigator.NamespaceURI);

            this.EntityDataMapper.MapOne(xPathNavigator, rarity);

            var actual = rarity.Name;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapMany() {
            var raritys = new List<IRarity>();
            var expected = this.XPathNavigators.Count;

            this.EntityDataMapper.MapMany(this.XPathNavigators, raritys);

            var actual = raritys.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}