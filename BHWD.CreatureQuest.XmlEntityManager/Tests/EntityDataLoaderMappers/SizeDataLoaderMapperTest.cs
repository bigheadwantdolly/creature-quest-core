﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataLoaderMappers {

    [TestFixture]
    internal class SizeDataLoaderMapperTest {

        private IEntityDataMapper<XPathNavigator, ISize> EntityDataMapper { get; set; }
        private List<XPathNavigator> XPathNavigators { get; set; }

        [SetUp]
        public void SetUp() {
            const string resourceName = "sizes.xml";
            const string rootNode = "sizes";

            var xmlResourceLoader = new XmlResourceLoader();
            var xPathDocument = xmlResourceLoader.Execute(resourceName);
            var entityDataLoaderIterator = new EntityDataLoaderIterator();
            var entityFactory = new EntityFactory();

            EntityFactory.Instance = entityFactory;
            entityFactory.Register<ISize>(typeof(Size));

            this.XPathNavigators = entityDataLoaderIterator.GetList(xPathDocument, rootNode);
            this.EntityDataMapper = new SizeDataMapper();
        }

        [Test]
        public void MapName() {
            var size = new Size();
            var xPathNavigator = this.XPathNavigators.FirstOrDefault();
            var expected = xPathNavigator?.GetAttribute(SizeDataMapper.SourceKeyName, xPathNavigator.NamespaceURI);

            this.EntityDataMapper.MapOne(xPathNavigator, size);

            var actual = size.Name;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapMany() {
            var sizes = new List<ISize>();
            var expected = this.XPathNavigators.Count;

            this.EntityDataMapper.MapMany(this.XPathNavigators, sizes);

            var actual = sizes.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}