﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataLoaderMappers {

    [TestFixture]
    internal class CreatureDataLoaderMapperTest {

        private IEntityDataMapper<XPathNavigator, ICreature> EntityDataMapper { get; set; }
        private List<XPathNavigator> XPathNavigators { get; set; }

        [SetUp]
        public void SetUp() {
            const string resourceName = "small-red-creatures.xml";
            const string rootNode = "creatures";

            var xmlResourceLoader = new XmlResourceLoader();
            var xPathDocument = xmlResourceLoader.Execute(resourceName);
            var entityDataLoaderIterator = new EntityDataLoaderIterator();
            var entityFactory = new EntityFactory();
            var colorDataSet = new StubEntityDataSet<IColor>(
                new List<IColor> {
                    new Color {
                        Name = "red"
                    }
                });
            var rarityDataSet = new StubEntityDataSet<IRarity>(
                new List<IRarity> {
                    new Rarity {
                        Name = "common"
                    }
                });
            var sizeDataSet = new StubEntityDataSet<ISize>(
                new List<ISize> {
                    new Size {
                        Name = "small"
                    }
                });

            EntityFactory.Instance = entityFactory;
            entityFactory.Register<ICreature>(typeof(Creature));

            this.XPathNavigators = entityDataLoaderIterator.GetList(xPathDocument, rootNode);
            this.EntityDataMapper = new CreatureDataMapper(colorDataSet, rarityDataSet, sizeDataSet);
        }

        [Test]
        public void MapColor() {
            var creature = new Creature();
            var xPathNavigator = this.XPathNavigators.FirstOrDefault();
            var expected = xPathNavigator?.GetAttribute(CreatureDataMapper.SourceKeyColor, xPathNavigator.NamespaceURI);

            this.EntityDataMapper.MapOne(xPathNavigator, creature);

            var actual = creature.Color.Name;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapName() {
            var creature = new Creature();
            var xPathNavigator = this.XPathNavigators.FirstOrDefault();
            var expected = xPathNavigator?.GetAttribute(CreatureDataMapper.SourceKeyName, xPathNavigator.NamespaceURI);

            this.EntityDataMapper.MapOne(xPathNavigator, creature);

            var actual = creature.Name;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapRarity() {
            var creature = new Creature();
            var xPathNavigator = this.XPathNavigators.FirstOrDefault();
            var expected = xPathNavigator?.GetAttribute(CreatureDataMapper.SourceKeyRarity, xPathNavigator.NamespaceURI);

            this.EntityDataMapper.MapOne(xPathNavigator, creature);

            var actual = creature.Rarity.Name;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapSize() {
            var creature = new Creature();
            var xPathNavigator = this.XPathNavigators.FirstOrDefault();
            var expected = xPathNavigator?.GetAttribute(CreatureDataMapper.SourceKeySize, xPathNavigator.NamespaceURI);

            this.EntityDataMapper.MapOne(xPathNavigator, creature);

            var actual = creature.Size.Name;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapMany() {
            var creatures = new List<ICreature>();
            var expected = this.XPathNavigators.Count;

            this.EntityDataMapper.MapMany(this.XPathNavigators, creatures);

            var actual = creatures.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}