﻿using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityDataSets;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityDataSets {

    [TestFixture]
    internal class EntityDataSetContainerTest {

        private IEntityDataSetContainer EntityDataSetContainer { get; set; }

        [SetUp]
        public void SetUp() {
            this.EntityDataSetContainer = new EntityDataSetContainer();
            InMemoryEntityGateway.EntityDataSets.EntityDataSetContainer.Instance = this.EntityDataSetContainer;
        }

        [Test]
        public void Instances() {
            var actual = InMemoryEntityGateway.EntityDataSets.EntityDataSetContainer.Instance;
            var expected = this.EntityDataSetContainer;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Registers() {
            var entityDataSet = new StubEntityDataSet<IDummyEntity>();
            var actual = this.EntityDataSetContainer.Register(entityDataSet);
            const bool expected = true;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Gets() {
            var entityDataSet = new StubEntityDataSet<IDummyEntity>();
            this.EntityDataSetContainer.Register(entityDataSet);
            var actual = this.EntityDataSetContainer.Get<IDummyEntity>();

            Assert.IsNotNull(actual);
        }

        [Test]
        public void TypesAreRetained() {
            var entityDataSet = new StubEntityDataSet<IDummyEntity>();
            this.EntityDataSetContainer.Register(entityDataSet);
            var actual = this.EntityDataSetContainer.Get<IDummyEntity>().GetType();
            var expected = entityDataSet.GetType();

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}