﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetManyAttackGatewayTest {

        private List<IAttack> Attacks { get; set; }
        private IGetManyEntityGateway<IAttack> GetManyAttackGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Attacks = new List<IAttack> {
                new Attack(),
                new Attack(),
                new Attack()
            };
            var entityDataSet = new StubEntityDataSet<IAttack>(this.Attacks);

            this.GetManyAttackGateway = new GetManyAttackGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetManyAttackGateway.GetAll().Count;
            var expected = this.Attacks.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}