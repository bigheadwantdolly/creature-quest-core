﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetManyCreatureGatewayTest {

        private List<ICreature> Creatures { get; set; }
        private IGetManyCreatureGateway GetManyCreatureGateway { get; set; }

        private readonly IColor Red = new Color {
            Id = Guid.NewGuid(),
            Name = "Red"
        };

        private readonly IColor Blue = new Color {
            Id = Guid.NewGuid(),
            Name = "Blue"
        };

        private readonly ISize Small = new Size {
            Id = Guid.NewGuid(),
            Name = "Small"
        };

        private readonly ISize Medium = new Size {
            Id = Guid.NewGuid(),
            Name = "Medium"
        };

        private readonly IEvolution FirstEvolution = new Evolution {
            Id = Guid.NewGuid(),
            Cardinality = 1
        };

        private readonly IEvolution SecondEvolution = new Evolution {
            Id = Guid.NewGuid(),
            Cardinality = 2
        };
        
        private readonly ILevel FirstLevel = new Level {
            Id = Guid.NewGuid(),
            Number = 1
        };
        
        private readonly ILevel SecondLevel = new Level {
            Id = Guid.NewGuid(),
            Number = 2
        };

        [SetUp]
        public void SetUp() {
            this.Creatures = new List<ICreature> {
                new Creature {
                    Color = this.Red,
                    Evolution = this.FirstEvolution,
                    Size = this.Small,
                    Level = this.FirstLevel,
                    Name = "larry"
                },
                new Creature {
                    Color = this.Blue,
                    Evolution = this.SecondEvolution,
                    Size = this.Medium,
                    Level = this.FirstLevel,
                    Name = "nick"
                },
                new Creature {
                    Color = this.Red,
                    Evolution = this.SecondEvolution,
                    Size = this.Small,
                    Level = this.SecondLevel,
                    Name = "sally"
                },
                new Creature(),
                new Creature()
            };
            var entityDataSet = new StubEntityDataSet<ICreature>(this.Creatures);

            this.GetManyCreatureGateway = new GetManyCreatureGateway(entityDataSet);
        }

        [Test]
        public void GetAllReturnsAllData() {
            var actual = this.GetManyCreatureGateway.GetAll().Count;
            var expected = this.Creatures.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void GetAllFiltersByColor() {
            var filters = new List<Guid> {
                this.Red.Id
            };
            var actual = this.GetManyCreatureGateway.GetAll(filters, null, null).Count;
            const int expected = 2;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void GetAllFiltersByEvolution() {
            var filters = new List<Guid> {
                this.FirstEvolution.Id
            };
            var actual = this.GetManyCreatureGateway.GetAll(null, filters, null).Count;
            const int expected = 1;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void GetAllFiltersBySize() {
            var filters = new List<Guid> {
                this.Small.Id
            };
            var actual = this.GetManyCreatureGateway.GetAll(null, null, filters).Count;
            const int expected = 2;

            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void GetAllFiltersByLevel() {
            var levelId = this.SecondLevel.Id;
            var actual = this.GetManyCreatureGateway.GetAll(null, null, null, levelId).Count;
            const int expected = 1;

            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void GetAllFiltersByName() {
            var name = this.Creatures.First().Name;
            var actual = this.GetManyCreatureGateway.GetAll(null, null, null, null, name).Count;
            const int expected = 1;

            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void GetAllFiltersByColorAndEvolution() {
            var colorFilters = new List<Guid> {
                this.Blue.Id,
                this.Red.Id
            };
            var evolutionFilters = new List<Guid> {
                this.SecondEvolution.Id
            };
            var actual = this.GetManyCreatureGateway.GetAll(colorFilters, evolutionFilters, null).Count;
            const int expected = 2;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void GetAllFiltersByEvolutionAndSize() {
            var evolutionFilters = new List<Guid> {
                this.FirstEvolution.Id,
                this.SecondEvolution.Id
            };
            var sizeFilters = new List<Guid> {
                this.Small.Id
            };
            var actual = this.GetManyCreatureGateway.GetAll(null, evolutionFilters, sizeFilters).Count;
            const int expected = 2;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void GetAllFiltersByColorAndEvolutionAndSize() {
            var colorFilters = new List<Guid> {
                this.Blue.Id,
                this.Red.Id
            };
            var evolutionFilters = new List<Guid> {
                this.FirstEvolution.Id,
                this.SecondEvolution.Id
            };
            var sizeFilters = new List<Guid> {
                this.Small.Id
            };
            var actual = this.GetManyCreatureGateway.GetAll(colorFilters, evolutionFilters, sizeFilters).Count;
            const int expected = 2;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}