﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetOneColorGatewayTest {

        private List<IColor> Colors { get; set; }
        private IGetOneEntityGateway<IColor> GetOneColorGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Colors = new List<IColor> {
                new Color {
                    Id = Guid.NewGuid(),
                    Name = "Mr Color"
                },
                new Color {
                    Id = Guid.NewGuid(),
                    Name = "Mrs Color"
                }
            };
            var entityDataSet = new StubEntityDataSet<IColor>(this.Colors);

            this.GetOneColorGateway = new GetOneColorGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var expected = this.Colors[0];
            var actual = this.GetOneColorGateway.GetById(expected.Id);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}