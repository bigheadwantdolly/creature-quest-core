﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetManyRarityGatewayTest {

        private List<IRarity> Rarities { get; set; }
        private IGetManyEntityGateway<IRarity> GetManyRarityGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Rarities = new List<IRarity> {
                new Rarity(),
                new Rarity(),
                new Rarity()
            };
            var entityDataSet = new StubEntityDataSet<IRarity>(this.Rarities);

            this.GetManyRarityGateway = new GetManyRarityGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetManyRarityGateway.GetAll().Count;
            var expected = this.Rarities.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}