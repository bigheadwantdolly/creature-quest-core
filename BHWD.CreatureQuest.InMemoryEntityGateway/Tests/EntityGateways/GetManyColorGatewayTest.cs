﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetManyColorGatewayTest {

        private List<IColor> Colors { get; set; }
        private IGetManyEntityGateway<IColor> GetManyColorGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Colors = new List<IColor> {
                new Color(),
                new Color(),
                new Color()
            };
            var entityDataSet = new StubEntityDataSet<IColor>(this.Colors);

            this.GetManyColorGateway = new GetManyColorGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetManyColorGateway.GetAll().Count;
            var expected = this.Colors.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}