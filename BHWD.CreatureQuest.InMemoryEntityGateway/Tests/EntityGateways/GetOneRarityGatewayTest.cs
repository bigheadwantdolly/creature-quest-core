﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetOneRarityGatewayTest {

        private List<IRarity> Rarities { get; set; }
        private IGetOneEntityGateway<IRarity> GetOneRarityGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Rarities = new List<IRarity> {
                new Rarity {
                    Id = Guid.NewGuid(),
                    Name = "Mr Rarity"
                },
                new Rarity {
                    Id = Guid.NewGuid(),
                    Name = "Mrs Rarity"
                }
            };
            var entityDataSet = new StubEntityDataSet<IRarity>(this.Rarities);

            this.GetOneRarityGateway = new GetOneRarityGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var expected = this.Rarities[0];
            var actual = this.GetOneRarityGateway.GetById(expected.Id);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}