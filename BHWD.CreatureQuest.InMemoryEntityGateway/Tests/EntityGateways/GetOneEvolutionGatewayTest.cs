﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetOneEvolutionGatewayTest {

        private List<IEvolution> Evolutions { get; set; }
        private IGetOneEntityGateway<IEvolution> GetOneEvolutionGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Evolutions = new List<IEvolution> {
                new Evolution {
                    Id = Guid.NewGuid()
                },
                new Evolution {
                    Id = Guid.NewGuid()
                }
            };
            var entityDataSet = new StubEntityDataSet<IEvolution>(this.Evolutions);

            this.GetOneEvolutionGateway = new GetOneEvolutionGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var expected = this.Evolutions[0];
            var actual = this.GetOneEvolutionGateway.GetById(expected.Id);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}