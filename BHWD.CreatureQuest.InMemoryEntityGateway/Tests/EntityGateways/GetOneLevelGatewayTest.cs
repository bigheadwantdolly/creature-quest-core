﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetOneLevelGatewayTest {

        private List<ILevel> Levels { get; set; }
        private IGetOneEntityGateway<ILevel> GetOneLevelGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Levels = new List<ILevel> {
                new Level {
                    Id = Guid.NewGuid()
                },
                new Level {
                    Id = Guid.NewGuid()
                }
            };
            var entityDataSet = new StubEntityDataSet<ILevel>(this.Levels);

            this.GetOneLevelGateway = new GetOneLevelGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var expected = this.Levels[0];
            var actual = this.GetOneLevelGateway.GetById(expected.Id);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}