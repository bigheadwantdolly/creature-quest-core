﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetManyEvolutionGatewayTest {

        private List<IEvolution> Evolutions { get; set; }
        private IGetManyEntityGateway<IEvolution> GetManyEvolutionGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Evolutions = new List<IEvolution> {
                new Evolution(),
                new Evolution(),
                new Evolution()
            };
            var entityDataSet = new StubEntityDataSet<IEvolution>(this.Evolutions);

            this.GetManyEvolutionGateway = new GetManyEvolutionGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetManyEvolutionGateway.GetAll().Count;
            var expected = this.Evolutions.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}