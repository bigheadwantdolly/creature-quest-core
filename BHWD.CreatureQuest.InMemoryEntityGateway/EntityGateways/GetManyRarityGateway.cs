﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetManyRarityGateway : IGetManyEntityGateway<IRarity> {

        private readonly IEntityDataSet<IRarity> EntityDataSet;

        public GetManyRarityGateway(IEntityDataSet<IRarity> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<IRarity> GetAll() {
            return this.EntityDataSet.Entities;
        }

    }

}