﻿using System;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetOneLevelGateway : IGetOneEntityGateway<ILevel> {

        private readonly IEntityDataSet<ILevel> EntityDataSet;

        public GetOneLevelGateway(IEntityDataSet<ILevel> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public ILevel GetById(Guid id) {
            return this.EntityDataSet.Entities.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}