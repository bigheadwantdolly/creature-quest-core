﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetManyCreatureGateway : IGetManyCreatureGateway {

        private readonly IEntityDataSet<ICreature> EntityDataSet;

        public GetManyCreatureGateway(IEntityDataSet<ICreature> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<ICreature> GetAll() {
            return this.EntityDataSet.Entities;
        }

        public List<ICreature> GetAll(List<Guid> colorIds, List<Guid> evolutionIds, List<Guid> sizeIds, Guid? levelId, string name) {
            return this.EntityDataSet.Entities.Where(entity => colorIds == null || colorIds.Count == 0 || (entity.Color != null && colorIds.Contains(entity.Color.Id)))
                .Where(entity => evolutionIds == null || evolutionIds.Count == 0 || (entity.Evolution != null && evolutionIds.Contains(entity.Evolution.Id)))
                .Where(entity => sizeIds == null || sizeIds.Count == 0 || (entity.Size != null && sizeIds.Contains(entity.Size.Id)))
                .Where(entity => levelId == null || levelId == Guid.Empty || (entity.Level != null && levelId.Equals(entity.Level.Id)))
                .Where(entity => name == null || string.IsNullOrWhiteSpace(name) || (entity.Name != null && name.Equals(entity.Name)))
                .ToList();
        }

    }

}