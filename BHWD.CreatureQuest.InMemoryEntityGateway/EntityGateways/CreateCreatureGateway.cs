﻿using System;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class CreateCreatureGateway : ICreateEntityGateway<ICreature> {

        private readonly IEntityDataSet<ICreature> EntityDataSet;

        public CreateCreatureGateway(IEntityDataSet<ICreature> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public ICreature Create(ICreature creature) {
            this.ValidateInput(creature);

            creature.Id = Guid.NewGuid();

            this.EntityDataSet.Entities.Add(creature);

            return creature;
        }

        private void ValidateInput(ICreature creature) {
            if (creature == null) {
                throw new ArgumentNullException($"Attempted to create `{nameof(creature)}`");
            }

            if (!creature.Id.Equals(Guid.Empty)) {
                throw new InvalidOperationException($"Cannot create a `{nameof(creature)}` that has already been assigned an `id`");
            }
        }

    }

}