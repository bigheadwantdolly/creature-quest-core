﻿using System;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetOneAttackGateway : IGetOneEntityGateway<IAttack> {

        private readonly IEntityDataSet<IAttack> EntityDataSet;

        public GetOneAttackGateway(IEntityDataSet<IAttack> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public IAttack GetById(Guid id) {
            return this.EntityDataSet.Entities.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}