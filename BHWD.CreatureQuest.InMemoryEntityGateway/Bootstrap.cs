﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.Entity.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityDataSets;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.Plugin;

namespace BHWD.CreatureQuest.InMemoryEntityGateway {

    public class Bootstrap : IPlugin {

        public string Description => "In Memory Data Store";

        public void BootstrapModule() {
            Console.WriteLine(this.Description + " Starting Up!");

            this.InitializeDataSets();
            this.InitializeEntityGateways();
            this.PopulateDataSets();
        }

        private void InitializeDataSets() {
            var attackDataSet = new EntityDataSet<IAttack>();
            var colorDataSet = new EntityDataSet<IColor>();
            var creatureDataSet = new EntityDataSet<ICreature>();
            var evolutionDataSet = new EntityDataSet<IEvolution>();
            var levelDataSet = new EntityDataSet<ILevel>();
            var rarityDataSet = new EntityDataSet<IRarity>();
            var sizeDataSet = new EntityDataSet<ISize>();

            EntityDataSetContainer.Instance.Register(attackDataSet);
            EntityDataSetContainer.Instance.Register(colorDataSet);
            EntityDataSetContainer.Instance.Register(creatureDataSet);
            EntityDataSetContainer.Instance.Register(evolutionDataSet);
            EntityDataSetContainer.Instance.Register(levelDataSet);
            EntityDataSetContainer.Instance.Register(rarityDataSet);
            EntityDataSetContainer.Instance.Register(sizeDataSet);
        }

        private void InitializeEntityGateways() {
            var attackDataSet = EntityDataSetContainer.Instance.Get<IAttack>();
            var getManyAttackGateway = new GetManyAttackGateway(attackDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<IAttack>>(getManyAttackGateway);
            var getOneAttackGateway = new GetOneAttackGateway(attackDataSet);
            EntityGatewayContainer.Instance.Register<IGetOneEntityGateway<IAttack>>(getOneAttackGateway);

            var colorDataSet = EntityDataSetContainer.Instance.Get<IColor>();
            var getManyColorGateway = new GetManyColorGateway(colorDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<IColor>>(getManyColorGateway);
            var getOneColorGateway = new GetOneColorGateway(colorDataSet);
            EntityGatewayContainer.Instance.Register<IGetOneEntityGateway<IColor>>(getOneColorGateway);

            var creatureDataSet = EntityDataSetContainer.Instance.Get<ICreature>();
            var createCreatureGateway = new CreateCreatureGateway(creatureDataSet);
            EntityGatewayContainer.Instance.Register<ICreateEntityGateway<ICreature>>(createCreatureGateway);
            var getManyCreatureGateway = new GetManyCreatureGateway(creatureDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyCreatureGateway>(getManyCreatureGateway);
            var getOneCreatureGateway = new GetOneCreatureGateway(creatureDataSet);
            EntityGatewayContainer.Instance.Register<IGetOneEntityGateway<ICreature>>(getOneCreatureGateway);

            var evolutionDataSet = EntityDataSetContainer.Instance.Get<IEvolution>();
            var getManyEvolutionGateway = new GetManyEvolutionGateway(evolutionDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<IEvolution>>(getManyEvolutionGateway);
            var getOneEvolutionGateway = new GetOneEvolutionGateway(evolutionDataSet);
            EntityGatewayContainer.Instance.Register<IGetOneEntityGateway<IEvolution>>(getOneEvolutionGateway);

            var levelDataSet = EntityDataSetContainer.Instance.Get<ILevel>();
            var getManyLevelGateway = new GetManyLevelGateway(levelDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<ILevel>>(getManyLevelGateway);
            var getOneLevelGateway = new GetOneLevelGateway(levelDataSet);
            EntityGatewayContainer.Instance.Register<IGetOneEntityGateway<ILevel>>(getOneLevelGateway);

            var rarityDataSet = EntityDataSetContainer.Instance.Get<IRarity>();
            var getManyRarityGateway = new GetManyRarityGateway(rarityDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<IRarity>>(getManyRarityGateway);
            var getOneRarityGateway = new GetOneRarityGateway(rarityDataSet);
            EntityGatewayContainer.Instance.Register<IGetOneEntityGateway<IRarity>>(getOneRarityGateway);

            var sizeDataSet = EntityDataSetContainer.Instance.Get<ISize>();
            var getManySizeGateway = new GetManySizeGateway(sizeDataSet);
            EntityGatewayContainer.Instance.Register<IGetManyEntityGateway<ISize>>(getManySizeGateway);
            var getOneSizeGateway = new GetOneSizeGateway(sizeDataSet);
            EntityGatewayContainer.Instance.Register<IGetOneEntityGateway<ISize>>(getOneSizeGateway);
        }

        private void PopulateDataSets() {
            this.PopulateAttackData();
            this.PopulateColorData();
            this.PopulateRarityData();
            this.PopulateSizeData();
            this.PopulateEvolutionData();
            this.PopulateLevelData();
        }

        private void PopulateAttackData() {
            var dataSet = EntityDataSetContainer.Instance.Get<IAttack>();
            var names = new List<string> {
                "grunt",
                "squire",
                "soldier",
                "assassian",
                "defender",
                "healer",
                "ranger",
                "bruiser",
                "mage",
                "hunter",
                "mystic",
                "scholar",
                "bard",
                "warlock",
                "berserker"
            };

            foreach (var name in names) {
                var entity = EntityFactory.Instance.Create<IAttack>();
                entity.Id = Guid.NewGuid();
                entity.Name = name;
                dataSet.Entities.Add(entity);
            }
        }

        private void PopulateColorData() {
            var dataSet = EntityDataSetContainer.Instance.Get<IColor>();
            var names = new List<string> {
                "blue",
                "red",
                "green",
                "yellow",
                "white",
                "black"
            };

            foreach (var name in names) {
                var entity = EntityFactory.Instance.Create<IColor>();
                entity.Id = Guid.NewGuid();
                entity.Name = name;
                dataSet.Entities.Add(entity);
            }
        }

        private void PopulateRarityData() {
            var dataSet = EntityDataSetContainer.Instance.Get<IRarity>();
            var names = new List<string> {
                "common",
                "rare",
                "epic",
                "legendary"
            };

            foreach (var name in names) {
                var entity = EntityFactory.Instance.Create<IRarity>();
                entity.Id = Guid.NewGuid();
                entity.Name = name;
                dataSet.Entities.Add(entity);
            }
        }

        private void PopulateSizeData() {
            var dataSet = EntityDataSetContainer.Instance.Get<ISize>();
            var names = new List<string> {
                "small",
                "medium",
                "large",
                "boss"
            };

            foreach (var name in names) {
                var entity = EntityFactory.Instance.Create<ISize>();
                entity.Id = Guid.NewGuid();
                entity.Name = name;
                dataSet.Entities.Add(entity);
            }
        }

        private void PopulateEvolutionData() {
            var dataSet = EntityDataSetContainer.Instance.Get<IEvolution>();
            var rarityDataSet = EntityDataSetContainer.Instance.Get<IRarity>();
            var sizeDataSet = EntityDataSetContainer.Instance.Get<ISize>();
            const string cardinalityKey = "cardinality";
            const string rarityKey = "rarities";
            const string sizeKey = "sizes";
            var evolutions = new List<Dictionary<string, object>> {
                new Dictionary<string, object> {
                    {cardinalityKey, 1}, {
                        rarityKey, new List<IRarity> {
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("common"))
                        }
                    }, {
                        sizeKey, new List<ISize> {
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("small"))
                        }
                    }
                },
                new Dictionary<string, object> {
                    {cardinalityKey, 2}, {
                        rarityKey, new List<IRarity> {
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("common")),
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("rare"))
                        }
                    }, {
                        sizeKey, new List<ISize> {
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("small")),
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("medium"))
                        }
                    }
                },
                new Dictionary<string, object> {
                    {cardinalityKey, 3}, {
                        rarityKey, new List<IRarity> {
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("common")),
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("rare")),
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("epic"))
                        }
                    }, {
                        sizeKey, new List<ISize> {
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("small")),
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("medium")),
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("large"))
                        }
                    }
                },
                new Dictionary<string, object> {
                    {cardinalityKey, 4}, {
                        rarityKey, new List<IRarity> {
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("rare")),
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("epic")),
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("legendary"))
                        }
                    }, {
                        sizeKey, new List<ISize> {
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("medium")),
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("large")),
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("boss"))
                        }
                    }
                },
                new Dictionary<string, object> {
                    {cardinalityKey, 5}, {
                        rarityKey, new List<IRarity> {
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("epic")),
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("legendary"))
                        }
                    }, {
                        sizeKey, new List<ISize> {
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("large")),
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("boss"))
                        }
                    }
                },
                new Dictionary<string, object> {
                    {cardinalityKey, 6}, {
                        rarityKey, new List<IRarity> {
                            rarityDataSet.Entities.FirstOrDefault(rarity => rarity.Name.Equals("legendary"))
                        }
                    }, {
                        sizeKey, new List<ISize> {
                            sizeDataSet.Entities.FirstOrDefault(size => size.Name.Equals("boss"))
                        }
                    }
                }
            };

            foreach (var evolution in evolutions) {
                var entity = EntityFactory.Instance.Create<IEvolution>();

                evolution.TryGetValue(cardinalityKey, out var cardinality);
                evolution.TryGetValue(rarityKey, out var rarities);
                evolution.TryGetValue(sizeKey, out var sizes);

                entity.Id = Guid.NewGuid();

                if (cardinality != null) {
                    entity.Cardinality = (int) cardinality;
                }

                entity.Rarities = rarities as List<IRarity>;
                entity.Sizes = sizes as List<ISize>;

                dataSet.Entities.Add(entity);
            }
        }

        private void PopulateLevelData() {
            var dataSet = EntityDataSetContainer.Instance.Get<ILevel>();
            var evolutionDataSet = EntityDataSetContainer.Instance.Get<IEvolution>();
            var sizeDataSet = EntityDataSetContainer.Instance.Get<ISize>();
            const string levelRequirementsKey = "levelRequirements";
            const string numberKey = "number";
            var levels = new List<Dictionary<string, object>> {
                new Dictionary<string, object> {
                    {numberKey, 15}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(1))
                            }
                        }
                    }
                },
                new Dictionary<string, object> {
                    {numberKey, 30}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(2))
                            }
                        }
                    }
                },
                new Dictionary<string, object> {
                    {numberKey, 45}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(3))
                            }
                        }
                    }
                },
                new Dictionary<string, object> {
                    {numberKey, 50}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(3)),
                                Size = sizeDataSet.Entities.FirstOrDefault(evolution => evolution.Name.Equals("small"))
                            }
                        }
                    }
                },
                new Dictionary<string, object> {
                    {numberKey, 55}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(3)),
                                Size = sizeDataSet.Entities.FirstOrDefault(evolution => evolution.Name.Equals("small"))
                            }
                        }
                    }
                },
                new Dictionary<string, object> {
                    {numberKey, 60}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(4))
                            },
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(3)),
                                Size = sizeDataSet.Entities.FirstOrDefault(evolution => evolution.Name.Equals("small"))
                            }
                        }
                    }
                },
                new Dictionary<string, object> {
                    {numberKey, 65}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(4)),
                                Size = sizeDataSet.Entities.FirstOrDefault(evolution => evolution.Name.Equals("medium"))
                            }
                        }
                    }
                },
                new Dictionary<string, object> {
                    {numberKey, 75}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(5))
                            },
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(4)),
                                Size = sizeDataSet.Entities.FirstOrDefault(evolution => evolution.Name.Equals("medium"))
                            }
                        }
                    }
                },
                new Dictionary<string, object> {
                    {numberKey, 90}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(6))
                            },
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(5)),
                                Size = sizeDataSet.Entities.FirstOrDefault(evolution => evolution.Name.Equals("large"))
                            }
                        }
                    }
                },
                new Dictionary<string, object> {
                    {numberKey, 105}, {
                        levelRequirementsKey, new List<ILevelRequirement> {
                            new LevelRequirement {
                                Id = Guid.NewGuid(),
                                Evolution = evolutionDataSet.Entities.FirstOrDefault(evolution => evolution.Cardinality.Equals(6)),
                                Size = sizeDataSet.Entities.FirstOrDefault(evolution => evolution.Name.Equals("boss"))
                            }
                        }
                    }
                }
            };

            foreach (var level in levels) {
                var entity = EntityFactory.Instance.Create<ILevel>();

                level.TryGetValue(levelRequirementsKey, out var levelRequirements);
                level.TryGetValue(numberKey, out var number);

                entity.Id = Guid.NewGuid();

                if (number != null) {
                    entity.Number = (int) number;
                }

                entity.LevelRequirements = levelRequirements as List<ILevelRequirement>;

                dataSet.Entities.Add(entity);
            }
        }

    }

}